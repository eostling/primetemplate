# Prime Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
## [1.1.6] - 2021-08-01 Rebuild
### Added
    - Typescript
    - React Query
    - MFW

### Changed
    - Removal of Mobx-State-Tree

### Fixed
    - Performance of Queue's
    - HTTP Network Request Caching

##[Upcoming Version] 
TODO: Final updates 


## [1.1.1] - 2021-08-01 Previous Versions
### Added
 -Mobx State Tree

### Changed
-N/A

### Fixed
-N/A