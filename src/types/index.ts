export interface User {
  username: string
  permissions: Array<string>
  role: string
}

export interface ConditionsToMeetInOrderToDisplay {
  hasPermissions: Array<string>
}

export interface DisplayProperties {
  isDisabled: boolean
  toolTipMessage: string
}

export interface Action {
  displayProperties: DisplayProperties
  conditionsToMeetInOrderToDisplay: ConditionsToMeetInOrderToDisplay
}

export interface AlarmDetails {
  batteryPercentage: number
  confidence: string
  credentials: string
  eventID: string
  personDetected: boolean | null
}

export interface NextValidActions {
  acknowledge: Action
  secure: Action
  access: Action
  acknowledgeUnknown: Action
}

export interface Alarm {
  alarmID: string
  label: string
  sensorName: string
  sensorType: string
  facilityID: string
  facilityName: string
  floor: number
  zone: number
  geohash: string
  alarmType: string
  priority: number
  acknowledgementTimeout: boolean
  active: boolean
  state: string
  lat: number
  lon: number
  remark?: string | null
  alarmTime: number
  additionalAlarmDetails: AlarmDetails
  nextValidActions: NextValidActions
}

export interface SensorType {
  id: string
  make: string
  model: string
  type: string
  description: string | null
  events: Array<string>
}

export interface Sensor {
  sensorName: string
  sensorType: string
  lat: number
  lon: number
  geohash: string
  zone: number
  facilityID: string
  floor: number
  assetNumber: string
  lastModified: number
  notes: string
  make: string
  model: string
  currentPageItems: Array<any>
}
export interface ToastProps {
  title: string
  description?: string
  status?: string
  duration?: number
  isClosable?: boolean
  position?: string
}

export interface Pages {
  currentPage: number
  size: number
  hasNext: () => boolean
  hasPrev: () => boolean
  changeSize: VoidFunction
  prev: VoidFunction
  pageCount: VoidFunction
  next: VoidFunction
}

export interface Facility {
  facilityID: string
  displayName: string
  lat: number
  lon: number
  numFloors: number
  geohash: string
  streetaddress: string
  cadFilenames: Object
  zoneID: number
  state: string
  count: number
  canAccess: boolean
  canSecure: boolean
}

export interface BlueForceDevices {
  facilityID: string
  displayName: string
  lat: number
  lon: number
  numFloors: number
  geohash: string
  streetaddress: string
  cadFilenames: Object
  zoneID: number
  state: string
  count: number
  canAccess: boolean
  canSecure: boolean
}
export interface ViewPort {
  latitude: number
  longitude: number
  zoom: number
  bearing: number
  pitch: number
}

export interface FloorPlan {
  type: string
  features: Array<any>
}

export interface Maps {
  viewport: ViewPort
  activeBuilding: string
  activeFloor: string
  floorPlan: FloorPlan
}
