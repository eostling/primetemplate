import sjArea from '../assets/sj_afb_area.json'
import sjBuildings from '../assets/sj_afb_buildings.json'
import sjRoads from '../assets/sj_afb_roads.json'

export const allMapSettings = {
  novetta: {
    locations: [
      {
        name: 'Novetta',
        latitude: 38.9376,
        longitude: -77.217302,
        zoom: 13,
      },
      {
        name: 'Logan Circle, DC',
        lat: 38.91,
        lon: -77.03,
        zoom: 18,
      },
    ],
    area: undefined,
    buildings: undefined,
    roads: undefined,
  },
  afsafb: {
    locations: [
      {
        name: 'afsafb',
        latitude: 38.92757,
        longitude: -77.2172,
        zoom: 13,
      },
    ],
    area: undefined,
    buildings: undefined,
    roads: undefined,
    maxBounds: {
      maxLatitude: 38.931967,
      minLatitude: 38.920874,
      maxLongitude: -77.216014,
      minLongitude: -77.233246,
    },
  },
  sjafb: {
    locations: [
      {
        name: 'SJ AFB',
        latitude: 35.348,
        longitude: -77.96,
        zoom: 13,
      },
    ],
    area: sjArea,
    buildings: sjBuildings,
    roads: sjRoads,
    maxBounds: {
      maxLatitude: 35.37,
      minLatitude: 35.33,
      maxLongitude: -77.93,
      minLongitude: -77.995,
    },
  },
  c3: {
    locations: [
      {
        name: 'C3TestBuilding',
        latitude: 30.5790058,
        longitude: -86.446609,
        zoom: 13,
      },
    ],
    area: undefined,
    buildings: undefined,
    roads: undefined,
  },
}
