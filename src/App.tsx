import React, { ReactElement } from 'react'
import { QueryClientProvider } from 'react-query'
import { queryClient } from './utils/generateQueryClient'
import Router from './components/Router/Router'
import { ReactQueryDevtools } from 'react-query/devtools'
import StyleProvider from './components/StyleProvider/StyleProvider'
import { socket, SocketContext } from './context/socket'
import './maplibre-gl.css'

const App = (): ReactElement => {
  return (
    <StyleProvider>
      <SocketContext.Provider value={socket}>
        <QueryClientProvider client={queryClient}>
          <Router />
          <ReactQueryDevtools position='bottom-right' />
        </QueryClientProvider>
      </SocketContext.Provider>
    </StyleProvider>
  )
}

export default App
