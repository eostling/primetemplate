//@ts-nocheck
import { useEffect, useRef, useState } from 'react'
import alarmSrc from '../../assets/alarm.wav'
import timeoutSrc from '../../assets/alarmTimeout.wav'
import { useAlarms } from '../useAlarms/useAlarms'

// Return functions to play an alarm, play a timeout, or stop all sounds
const useAlarmAudio = () => {
  const { activeAlarms } = useAlarms()
  const [type, setType] = useState<string | null>(null)
  const alarmInterval = useRef<NodeJS.Timeout>()
  const timeoutInterval = useRef<NodeJS.Timeout>()
  const [unacknowledgedTime, setUnacknowledgedTime] = useState<number>(0)

  useEffect(() => {
    if (type != null) {
      const timeout = setInterval(() => {
        setUnacknowledgedTime(unacknowledgedTime + 1)
      }, 1000)

      return () => clearInterval(timeout)
    }
  })

  const alarmStateAlarms = activeAlarms.filter(
    (alarm) => alarm.state === 'ALARM'
  )

  useEffect(() => {
    if (unacknowledgedTime > 60 && alarmStateAlarms.length > 0) {
      setType('timeout')
    } else if (alarmStateAlarms.length > 0) {
      setType('alarm')
    } else {
      setType(null)
      setUnacknowledgedTime(0)
    }
  }, [alarmStateAlarms, unacknowledgedTime])

  function stopAll(alarmSound, timeoutSound) {
    alarmSound.pause()
    timeoutSound.pause()
    clearInterval(alarmInterval.current)
    clearInterval(timeoutInterval.current)
    alarmInterval.current = null
    timeoutInterval.current = null
  }

  useEffect(() => {
    const ALARM_INTERVAL = parseInt(
      (process.env.REACT_APP_ALARM_INTERVAL as unknown as string) || '1'
    )
    const alarmSound = new Audio(alarmSrc)
    const timeoutSound = new Audio(timeoutSrc)

    if (type === 'alarm' && !alarmInterval.current) {
      alarmInterval.current = setInterval(() => {
        alarmSound.play()
      }, ALARM_INTERVAL)
      alarmSound.play()
    }
    if (type === 'timeout' && !timeoutInterval.current) {
      alarmSound.pause()
      clearInterval(alarmInterval.current)

      timeoutInterval.current = setInterval(() => {
        timeoutSound.play()
      }, ALARM_INTERVAL)
      timeoutSound.play()
    }
    if (!type) {
      stopAll(alarmSound, timeoutSound)
    }

    // On teardown, stop all the sounds
    return () => {
      stopAll(alarmSound, timeoutSound)
    }
  }, [type])
}

export default useAlarmAudio
