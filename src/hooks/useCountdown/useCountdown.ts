import { useEffect, useState } from 'react'

const useCountdown = (countdownTime) => {
  const [timer, setTimer] = useState(countdownTime)
  const events = ['mousemove', 'keypress']

  useEffect(() => {
    const myInterval = setInterval(() => {
      if (timer > 0) {
        setTimer(timer - 1)
      }
    }, 1000)
    const resetTimeout = () => {
      setTimer(countdownTime)
    }
    for (let i in events) {
      window.addEventListener(events[i], resetTimeout)
    }
    return () => {
      clearInterval(myInterval)
      for (let i in events) {
        window.removeEventListener(events[i], resetTimeout)
      }
    }
  })
  return timer
}

export default useCountdown
