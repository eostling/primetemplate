import usePulseAnimation from './usePulseAnimation'
import { renderHook } from '@testing-library/react-hooks'

test('should test custom hook', () => {
  const { result } = renderHook(() => usePulseAnimation())

  expect(result.current).not.toBe(undefined)
})
