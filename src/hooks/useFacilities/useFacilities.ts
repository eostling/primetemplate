import axios from 'axios'
import { useQuery } from 'react-query'
import { Facility } from '../../types'

const getActiveFacilities = async (): Promise<Facility[]> => {
  const response = await axios.get('/api/facilities?onlyWithSensors=true')
  return response.data.facilities
}

const getAllFacilities = async (): Promise<Facility[]> => {
  const response = await axios.get('/api/facilities')
  return response.data.facilities
}

const sortFacilities = (facilities: Facility[]) => {
  return facilities
    .slice()
    .sort((a, b) => parseInt(a.facilityID) - parseInt(b.facilityID))
}

interface UseFacilitiesProps {
  activeFacilities: Facility[]
  allFacilities: Facility[]
}

export const useFacilities = (): UseFacilitiesProps => {
  const fallback: Facility[] = []

  const { data: activeFacilities = fallback } = useQuery(
    'activeFacilities',
    getActiveFacilities,
    { select: sortFacilities }
  )

  const { data: allFacilities = fallback } = useQuery(
    'allFacilities',
    getAllFacilities,
    { select: sortFacilities }
  )

  return { activeFacilities, allFacilities }
}
