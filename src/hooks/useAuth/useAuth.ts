import axios from 'axios'
import { useState } from 'react'
import { useMutation } from 'react-query'
import { useNavigate } from 'react-router-dom'

const LOCALSTORAGE_KEY = 'BDOC_user'

interface LoginProps {
  username: string
  password: string
}

export const useAuth = () => {
  const navigate = useNavigate()
  const [user, setUser] = useState(
    JSON.parse(window.localStorage.getItem(LOCALSTORAGE_KEY) as string)
  )

  const loginUser = async (credentials: LoginProps): Promise<void> => {
    const {
      data: { user },
    } = await axios.post('/api/authenticate', credentials)
    setUser(user)
    const token = JSON.stringify(user)
    window.localStorage.setItem(LOCALSTORAGE_KEY, token)
  }

  const {
    mutate: handleLogin,
    isError: isLoginError,
    isLoading: isLoginLoading,
  } = useMutation(loginUser, {
    onSuccess: () => {
      navigate('/', { replace: true })
    },
  })

  const logoutUser = async (): Promise<void> => {
    await axios.post('/api/logout')
    window.localStorage.removeItem(LOCALSTORAGE_KEY)
  }

  const { mutate: handleLogout } = useMutation(logoutUser, {
    onSuccess: () => {
      navigate('/login', { replace: true })
    },
  })

  const refreshUser = async (): Promise<void> => {
    const { data } = await axios.post('/api/authenticate/refresh')
    return data
  }

  const isLoggedIn =
    JSON.parse(window.localStorage.getItem(LOCALSTORAGE_KEY) as string) !== null

  return {
    user,
    isLoggedIn,
    handleLogin,
    handleLogout,
    refreshUser,
    isLoginError,
    isLoginLoading,
  }
}
