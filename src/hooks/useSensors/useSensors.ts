import axios from 'axios'
import { useQuery } from 'react-query'
import { Sensor } from '../../types'

const getSensors = async () => {
  const {
    data: { sensorTypes },
  } = await axios.get('/api/sensors')
  return sensorTypes
}

export const useSensors = () => {
  const fallback: Sensor[] = []
  const { data = fallback } = useQuery('sensors', getSensors)
  return data
}
