import useDropdown from './useDropdown'
import { renderHook } from '@testing-library/react-hooks'

test('should test custom hook', () => {
  const optionsArr: any[] = []
  const { result } = renderHook(() =>
    useDropdown('Mock Label', 'State', optionsArr)
  )
  expect(result.current).not.toBe(undefined)
})
