import { useState } from 'react'
import { Select } from '@chakra-ui/react'

const useDropdown = (
  label: string,
  defaultState: string | number | readonly string[],
  options: any[]
) => {
  const [state, updateState] = useState(defaultState)
  const id = `use-dropdown-${label.replace(' ', '').toLowerCase()}`
  const Dropdown = () => (
    <label htmlFor={id}>
      {label}
      <Select
        id={id}
        value={state}
        onChange={(e) => updateState(e.target.value)}
        onBlur={(e) => updateState(e.target.value)}
        disabled={!options.length}
      >
        <option />
        {options.map((item) => (
          <option key={item} value={item}>
            {item}
          </option>
        ))}
      </Select>
    </label>
  )
  return [state, Dropdown, updateState]
}

export default useDropdown
