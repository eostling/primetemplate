import axios from 'axios'
import {
  UseMutateFunction,
  useMutation,
  useQuery,
  useQueryClient,
} from 'react-query'
import { Alarm } from '../../types'
import getAlarmLabel from '../../utils/getAlarmLabel/getAlarmLabel'

const filterActiveAlarms = (alarms: Alarm[]): Alarm[] => {
  return alarms
    .filter((alarm) => alarm.state !== 'ACCESS' && alarm.state !== 'SECURE')
    .map((alarm) => ({ ...alarm, label: getAlarmLabel(alarm) }))
}

const filterAccessAlarms = (alarms: Alarm[]): Alarm[] => {
  return alarms
    .filter((alarm) => alarm.state === 'ACCESS')
    .map((alarm) => ({ ...alarm, label: getAlarmLabel(alarm) }))
}

const getAlarms = async (): Promise<Alarm[]> => {
  const response = await axios.get('/api/alarms')
  return response.data
}

const updateAlarmState = async (
  newState: string,
  alarms: Alarm[]
): Promise<any> => {
  const updateURL = `/api/alarms/actions/${newState}/`
  const response = await axios.put(updateURL, {
    alarms,
  })
  return response.data
}

const secureAlarm = async (alarms, remark, assessment): Promise<any> => {
  const updateURL = `/api/alarms/actions/secure/`

  const response = await axios.put(updateURL, {
    alarms,
    remark,
    assessment,
  })
  return response.data
}

interface UseAlarmsProps {
  activeAlarms: Alarm[]
  accessAlarms: Alarm[]
  allAlarms: Alarm[]
  handleMultiAlarmAcknowledge: UseMutateFunction<any, unknown, string, unknown>
  handleSingleAlarmState: UseMutateFunction<
    any,
    unknown,
    HandleSingleAlarmStateProps,
    unknown
  >
  handleAlarmSecure: UseMutateFunction<
    any,
    unknown,
    HandleAlarmSecureProps,
    unknown
  >
}

interface HandleSingleAlarmStateProps {
  newState: string
  alarm: Alarm
}

interface HandleAlarmSecureProps {
  alarms: Alarm[]
  remark: string
  assessment: string
}

export const useAlarms = (): UseAlarmsProps => {
  const fallback: Alarm[] = []
  const queryClient = useQueryClient()

  const { data: activeAlarms = fallback } = useQuery(
    'activeAlarms',
    getAlarms,
    {
      select: filterActiveAlarms,
    }
  )

  const { data: accessAlarms = fallback } = useQuery(
    'accessAlarms',
    getAlarms,
    {
      select: filterAccessAlarms,
    }
  )

  const { data: allAlarms = fallback } = useQuery('allAlarms', getAlarms)

  const { mutate: handleMultiAlarmAcknowledge } = useMutation(
    (newState: string) => updateAlarmState(newState, activeAlarms),
    {
      onSuccess: () => {
        queryClient.invalidateQueries('activeAlarms')
      },
    }
  )

  const getNextState = (currentState) => {
    switch (currentState) {
      case 'ALARM':
        return 'ACKNOWLEDGED'
      case 'ACKNOWLEDGED':
        return 'ACCESS'
      case 'UNKNOWN':
        return 'ACK_UNKNOWN'
      default:
        break
    }
  }

  const getNextValidAction = (currentState, alarmType) => {
    switch (currentState) {
      case 'ALARM':
        return {
          secure: {
            displayProperties: {
              isDisabled: true,
              toolTipMessage: 'Sensor is still reporting the alarm is high!',
            },
            conditionsToMeetInOrderToDisplay: {},
          },
          access: {
            displayProperties: {
              isDisabled: alarmType === 'INTRUSION' ? false : true,
              toolTipMessage: 'Put into Access',
            },
            conditionsToMeetInOrderToDisplay: {
              hasPermissions: ['ACCESS'],
            },
          },
        }
      case 'ACKNOWLEDGED':
        return {
          secure: {
            displayProperties: {
              isDisabled: true,
              toolTipMessage: 'Sensor is still reporting the alarm is high!',
            },
            conditionsToMeetInOrderToDisplay: {},
          },
        }
      case 'UNKNOWN':
        return {}

      default:
        break
    }
  }

  const { mutate: handleSingleAlarmState } = useMutation(
    ({ newState, alarm }: HandleSingleAlarmStateProps) =>
      updateAlarmState(newState, [alarm]),
    {
      onMutate: async (mutation) => {
        queryClient.cancelQueries('activeAlarms')
        queryClient.cancelQueries('accessAlarms')
        const previousData = queryClient.getQueryData('activeAlarms')

        queryClient.setQueryData('activeAlarms', (prevState: any) =>
          prevState.map((alarm) => {
            if (alarm.alarmID === mutation.alarm.alarmID) {
              return {
                ...alarm,
                state: getNextState(alarm.state),
                nextValidActions: getNextValidAction(
                  alarm.state,
                  alarm.alarmType
                ),
              }
            }
            return alarm
          })
        )
        return { previousData }
      },
      onSuccess: () => {
        queryClient.invalidateQueries('activeAlarms')
        queryClient.invalidateQueries('accessAlarms')
      },
    }
  )

  const { mutate: handleAlarmSecure } = useMutation(
    ({ alarms, remark, assessment }: HandleAlarmSecureProps) =>
      secureAlarm(alarms, remark, assessment),
    {
      onSuccess: () => {
        queryClient.invalidateQueries('activeAlarms')
        queryClient.invalidateQueries('accessAlarms')
      },
    }
  )

  return {
    activeAlarms,
    accessAlarms,
    allAlarms,
    handleMultiAlarmAcknowledge,
    handleSingleAlarmState,
    handleAlarmSecure,
  }
}
