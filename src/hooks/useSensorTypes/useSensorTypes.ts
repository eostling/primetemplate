import axios from 'axios'
import { useQuery } from 'react-query'
import { SensorType } from '../../types'

const getSensorTypes = async () => {
  const {
    data: { sensorTypes },
  } = await axios.get('/api/sensorTypes')
  return sensorTypes
}

export const useSensorTypes = () => {
  const fallback: SensorType[] = []
  const { data = fallback } = useQuery('sensorTypes', getSensorTypes)
  return data
}
