export const DEFAULT_MAP_STYLE_KEY = 'satellite'

export const maxZoom = 20

export const minZoom = 13

export const STATE_ORDER = [
  'ALARM',
  'UNKNOWN',
  'ACKNOWLEDGED',
  'ACK_UNKNOWN',
  'ACCESS',
  'SECURE',
]

export const STATE_TRANSITIONS = [
  ['ALARM', 'ACKNOWLEDGED'],
  ['ACKNOWLEDGED', 'ACCESS'],
  ['ACKNOWLEDGED', 'SECURE'],
  ['SECURE', 'ACCESS'],
  ['ACCESS', 'SECURE'],
  ['UNKNOWN', 'ACK_UNKNOWN'],
]
