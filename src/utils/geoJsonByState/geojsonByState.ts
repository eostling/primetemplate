import genPointGeoJson from '../genPointGeoJson/genPointGeoJson'

export const geojsonByState = (state: string, sensors: any, alarms: any) => {
  const markers = alarms.filter((alarm) => state.includes(alarm.state))
  return genPointGeoJson(markers, sensors)
}
