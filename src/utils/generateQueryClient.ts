import { QueryClient } from 'react-query'

const staleTime = Infinity

// generates new queryClient with default options
export const generateQueryClient = (): QueryClient => {
  return new QueryClient({
    defaultOptions: {
      queries: {
        staleTime,
        refetchOnWindowFocus: false, // does not refetch when refocusing window
      },
    },
  })
}

export const queryClient = generateQueryClient()
