import { STATE_ORDER } from '../../constants'

const sortAlarms = (alarms: any) => {
  return alarms.slice().sort((a: any, b: any) => {
    const stateA = STATE_ORDER.indexOf(a.state)
    const stateB = STATE_ORDER.indexOf(b.state)
    if (stateA !== stateB) {
      return stateA < stateB ? -1 : 1
    }
    if (a.priority !== b.priority) {
      return a.priority < b.priority ? -1 : 1
    }
    if (a.alarmTime !== b.alarmTime) {
      return a.alarmTime < b.alarmTime ? -1 : 1
    }
    if (a.facilityID !== b.facilityID) {
      return a.facilityID < b.facilityID ? -1 : 1
    }
    return 0
  })
}

export default sortAlarms
