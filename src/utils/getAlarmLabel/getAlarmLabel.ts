import { Alarm } from '../../types'
import labelify from '../labelify/labelify'

const getAlarmLabel = (alarm: Partial<Alarm>): string => {
  if (alarm && alarm.sensorName && alarm.alarmType) {
    return `${labelify(alarm.sensorName)} - ${labelify(alarm.alarmType)}`
  }
  return 'UNKNOWN ALARM'
}

export default getAlarmLabel
