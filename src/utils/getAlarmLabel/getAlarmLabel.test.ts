import getAlarmLabel from './getAlarmLabel'

const mockAlarm = {
  sensorName: 'testThing1',
  alarmType: 'INTRUSION',
}

const mockAlarmEmpty = {}

describe('Alarm label util', () => {
  test('get the label for an alarm', () => {
    const alarmLabel = getAlarmLabel(mockAlarm)
    expect(alarmLabel).toBe('testThing1 - INTRUSION')
  })

  test('returns unknown for invalid alarm', () => {
    const alarmLabel = getAlarmLabel(mockAlarmEmpty)
    expect(alarmLabel).toBe('UNKNOWN ALARM')
  })
})
