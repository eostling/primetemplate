const titleCase = (title: string): string => {
  const tokens = title.toLowerCase().split(' ')
  const upperTokens = tokens.map((token) => {
    return `${token.charAt(0).toUpperCase()}${token.slice(1)}`
  })
  return upperTokens.join(' ')
}

export default titleCase
