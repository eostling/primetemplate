import titleCase from './titleCase'

describe('titlecase util', () => {
  test('convert string to titleCase', () => {
    const label = titleCase('this is a test')
    expect(label).toBe('This Is A Test')
  })
})
