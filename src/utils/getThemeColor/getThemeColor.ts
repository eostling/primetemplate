import theme from '../../theme'

const getThemeColor = (selector: any) => {
  const { colors } = theme

  try {
    return selector
      .split('.')
      .reduce((obj: any, idx: number) => obj[idx], colors)
      .toLowerCase()
  } catch (e) {
    return '#000000'
  }
}

export default getThemeColor
