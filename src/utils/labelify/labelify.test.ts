import labelify from './labelify'

describe('Labelify util', () => {
  test('convert string to a valid label', () => {
    const label = labelify('test-Thing_1')
    expect(label).toBe('test Thing 1')
  })
})
