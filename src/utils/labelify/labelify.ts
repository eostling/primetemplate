const labelify = (str: string): string => {
  return str.replace(/[_-]/g, ' ')
}

export default labelify
