import genAlarmMetadata from './genAlarmMetadata'

const mockAlarms = [
  {
    alarmTime: -22102800000,
    priority: 1,
    zone: 7,
    facilityID: '6669',
    floor: 13,
  },
  {},
]

const mockSensorType = {
  id: 'testId',
  make: 'testMake',
  model: 'testModel',
  type: 'Camera',
  description: null,
  events: [],
}

describe('Generate alarm metadata util', () => {
  const alarmMetadata = mockAlarms.map((alarm) =>
    genAlarmMetadata(alarm, mockSensorType)
  )
  test('generates metadata', () => {
    expect(alarmMetadata[0]).toBe(
      'Priority 1 @ 4/20/1969 04:20:00\nZone 7 - Facility 6669 (Floor 13)\nSensor Type: Camera'
    )
  })
  test('formats missing information', () => {
    expect(alarmMetadata[1]).toBe(
      'Priority UNK @ DATE UNK TIME UNK\nZone UNK - Facility UNK (Floor UNK)\nSensor Type: Camera'
    )
  })
})
