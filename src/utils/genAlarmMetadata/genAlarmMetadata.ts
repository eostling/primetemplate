import formatDateShort from '../formatDateShort/formatDateShort'
import formatTime24 from '../formatDate24/formatDate24'
import { SensorType } from '../../types'

const genAlarmMetadata = (
  {
    alarmTime,
    facilityID = 'UNK',
    floor = 'UNK',
    priority = 'UNK',
    zone = 'UNK',
  }: {
    alarmTime?: number
    facilityID?: string | undefined
    floor?: number | string | undefined
    priority?: number | string | undefined
    zone?: number | string | undefined
  },
  sensorType?: SensorType
) => {
  const date = alarmTime ? formatDateShort(alarmTime) : 'DATE UNK'
  const time = alarmTime ? formatTime24(alarmTime) : 'TIME UNK'

  const priorityLabel = `Priority ${priority}`
  const timeLabel = `@ ${date} ${time}`
  const zoneLabel = `Zone ${zone}`
  const facilityLabel = `Facility ${facilityID} (Floor ${floor})`
  const sensorTypeLabel = `Sensor Type: ${sensorType ? sensorType.type : 'UNK'}`
  return `${`${priorityLabel} ${timeLabel}\n${zoneLabel} - ${facilityLabel}\n${sensorTypeLabel}`}`
}

export default genAlarmMetadata
