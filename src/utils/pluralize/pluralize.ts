const hm: any = { facility: 'facilities' }

const pluralize = (input: string) => (count: number) => {
  const subject = input.toLowerCase()
  if (count === 1 || count === undefined) return subject
  if (Object.keys(hm).includes(subject)) {
    return hm[subject]
  }
  return `${subject}s`
}

export default pluralize
