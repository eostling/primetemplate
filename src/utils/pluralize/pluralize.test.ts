import pluralize from './pluralize'

describe('Pluralize util', () => {
  test('uses plural map', () => {
    const pluralizeFacility = pluralize('facility')
    const facilityNone = pluralizeFacility(0)
    const facilitySingular = pluralizeFacility(1)
    const facilityPlural = pluralizeFacility(2)
    expect(facilityNone).toBe('facilities')
    expect(facilitySingular).toBe('facility')
    expect(facilityPlural).toBe('facilities')
  })
  test('handles non-mapped words', () => {
    const pluralizeAlarm = pluralize('alarm')
    const alarmNone = pluralizeAlarm(0)
    const alarmSingular = pluralizeAlarm(1)
    const alarmPlural = pluralizeAlarm(2)
    expect(alarmNone).toBe('alarms')
    expect(alarmSingular).toBe('alarm')
    expect(alarmPlural).toBe('alarms')
  })
  test('is case insensitive', () => {
    const caseInsensitiveResult = pluralize('Alarm')(1)
    expect(caseInsensitiveResult).toBe('alarm')
  })
})
