import formatTime24 from './formatDate24'

describe('24 hour time format util', () => {
  test('formats epoch date to luxon 24 hour format', () => {
    const date = 1641683919000
    const formattedTime = formatTime24(date)
    expect(formattedTime).toBe('23:18:39')
  })
})
