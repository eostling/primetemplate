import { DateTime } from 'luxon'
import formatDate from '../formatDate/formatDate'

// Given a PAPI date string, format to 24HR time
const formatTime24 = (date: number): string => {
  return formatDate(date, DateTime.TIME_24_WITH_SECONDS)
}

export default formatTime24
