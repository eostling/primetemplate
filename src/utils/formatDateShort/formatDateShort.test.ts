import formatDateShort from './formatDateShort'

describe('Short date format util', () => {
  test('formats epoch date to luxon medium format', () => {
    const date = 1641683919000
    const formattedDate = formatDateShort(date)
    expect(formattedDate).toBe('1/8/2022')
  })
})
