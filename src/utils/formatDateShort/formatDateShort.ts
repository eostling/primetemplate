import { DateTime } from 'luxon'
import formatDate from '../formatDate/formatDate'

// Given a PAPI date string, format to medium length date with weekday
export default function formatDateShort(date: number): string {
  return formatDate(date, DateTime.DATE_SHORT)
}
