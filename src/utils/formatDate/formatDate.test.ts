import formatDate from './formatDate'

describe('Format date util', () => {
  test('formats epoch date to luxon medium format', () => {
    const date = 1641683919000
    const formattedDate = formatDate(date)
    expect(formattedDate).toBe('1/8/2022')
  })
})
