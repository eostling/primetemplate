import { DateTime, DateTimeFormatPreset } from 'luxon'

// Given a PAPI date and output format, format the date
const formatDate = (date: number, toFormat?: DateTimeFormatPreset): string => {
  const dateTime = DateTime.fromMillis(date)
  return dateTime.setLocale('en-us').toLocaleString(toFormat)
}

export default formatDate
