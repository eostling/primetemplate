export const getLatLon = (sensors, alarm) => {
  if (sensors) {
    const match = sensors.find(
      (sensor) =>
        sensor.sensorType === alarm.sensorType &&
        sensor.sensorName === alarm.sensorName
    )
    if (match) {
      return [match.lon, match.lat]
    }
  }
  return [alarm.lon, alarm.lat]
}
