const groupBy = async (arr: any[], property: string) => {
  return arr.reduce(function (sensors: any, x: any) {
    const memo = sensors
    if (!memo[x[property]]) {
      memo[x[property]] = []
    }
    memo[x[property]].push(x)
    return memo
  }, {})
}

export default groupBy
