export default function genBlueForceGeoJson(items: any) {
  const features = items.map((BlueForceDevice: any) => ({
    type: 'Feature',
    id: BlueForceDevice.sensorName,
    properties: {
      displayName: BlueForceDevice.sensorName,
    },
    geometry: {
      type: 'Point',
      coordinates: [BlueForceDevice.longitude, BlueForceDevice.latitude],
    },
  }))
  return {
    type: 'FeatureCollection',
    features,
  }
}
