import { Alarm } from '../../types'
import { getLatLon } from '../getLatLon/GetLatLon'

interface GeoJsonProps {
  items: Array<Partial<Alarm>>
  sensors: any
}

export const genPointGeoJson = (items, sensors: GeoJsonProps) => {
  const features = items.map((alarm: any) => ({
    type: 'Feature',
    id: alarm.alarmID,
    properties: {
      ...alarm,
    },
    geometry: {
      type: 'Point',
      coordinates: getLatLon(sensors, alarm),
    },
  }))

  return {
    type: 'FeatureCollection',
    features,
  }
}

export default genPointGeoJson
