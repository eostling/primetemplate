import { render, RenderResult } from '@testing-library/react'
import { ReactElement } from 'react'
import { QueryClientProvider, setLogger } from 'react-query'
import { generateQueryClient } from '../utils/generateQueryClient'
import StyleProvider from '../components/StyleProvider/StyleProvider'
import { socket, SocketContext } from '../context/socket'

setLogger({
  log: console.log,
  warn: console.log,
  error: () => {
    //swallow react-query errors without printing out to console
  },
})

// Function to generate a unique query client for each test
const generateTestQueryClient = () => {
  const client = generateQueryClient()
  const options = client.getDefaultOptions()
  options.queries = { ...options.queries, retry: false }
  return client
}

export const renderWithProviders = (component: ReactElement): RenderResult => {
  const queryClient = generateTestQueryClient()
  return render(
    <StyleProvider>
      <SocketContext.Provider value={socket}>
        <QueryClientProvider client={queryClient}>
          {component}
        </QueryClientProvider>
      </SocketContext.Provider>
    </StyleProvider>
  )
}
