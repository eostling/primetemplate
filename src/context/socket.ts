import { createContext } from 'react'
import io, { Socket } from 'socket.io-client'

export const socket = io(process.env.REACT_APP_PAPI_URL as string, {
  transports: ['websocket'],
})

export const SocketContext = createContext<Socket>(undefined!)
