const { createProxyMiddleware } = require('http-proxy-middleware')

const PAPI_URL = process.env.REACT_APP_PAPI_URL || 'http://localhost:7000'
const VERIFY_PAPI_CERTS = process.env.NODE_TLS_REJECT_UNAUTHORIZED === '1'

module.exports = function setupProxy(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: PAPI_URL,
      changeOrigin: true,
      secure: VERIFY_PAPI_CERTS,
    })
  )
}
