import '@testing-library/jest-dom'

/* testing library doesn't support playback operations. This line prevents unnecessary console errors during testing from the Audio Hook. Does not affect test pass/failure */
window.HTMLMediaElement.prototype.pause = () => {
  /* do nothing */
}
