import React, { ReactElement, useState, ChangeEvent, useEffect } from 'react'
import {
  Box,
  Flex,
  Heading,
  Alert,
  Divider,
  AlertIcon,
  Stack,
  FormControl,
  FormErrorMessage,
  Button,
  InputGroup,
  InputLeftElement,
  Input,
  Spinner,
  Text,
  Checkbox,
  UnorderedList,
  ListItem,
} from '@chakra-ui/react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { AccountIcon, AirForceIcon, LockIcon } from '../../utils/generateIcons'
import { useAuth } from '../../hooks/useAuth/useAuth'

interface LoginProps {
  username: string
  password: string
}

const LoginPage = (): ReactElement => {
  const badgeRadius = 3
  const [accepted, setAccepted] = useState<boolean>(false)

  const {
    handleSubmit,
    register,
    reset,
    formState: { isSubmitting, errors },
  } = useForm()

  const { handleLogin, isLoginError, isLoginLoading } = useAuth()

  useEffect(() => {
    if (isLoginError) {
      reset()
    }
  }, [isLoginError, reset])

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setAccepted(e.target.checked)
  }

  return (
    <Flex
      minHeight='100vh'
      width='100vw'
      overflowX='hidden'
      overflowY='auto'
      flexDirection='column'
      alignItems='center'
      justifyContent='center'
      paddingBottom='10px'
      bgGradient='linear(135deg, gray.700, gray.900)'
    >
      <Flex
        alignItems='center'
        justifyContent='center'
        bg='gray.800'
        height={`${badgeRadius * 2}em`}
        width={`${badgeRadius * 2}em`}
        borderRadius='50%'
        marginTop={2}
        mb={`-${badgeRadius}em`}
        shadow='sm'
        zIndex='docked'
      >
        <AirForceIcon
          height={`${badgeRadius + 1}em`}
          width={`${badgeRadius + 1}em`}
          mt={`${badgeRadius / 6}em`}
        />
      </Flex>
      <Box
        w={{ base: '90%', xl: '72em' }}
        px={8}
        py={4}
        pt={`${badgeRadius + 1}em`}
        color='gray.100'
        bg='gray.700'
        borderRadius='md'
        shadow='lg'
      >
        <Stack spacing={4}>
          <Heading
            as='h1'
            textAlign='center'
            textTransform='uppercase'
            letterSpacing='0.15em'
          >
            Prime
          </Heading>
          <Heading as='h2' fontSize='md' color='gray.300' textAlign='center'>
            Base Defense Operations Center
          </Heading>
          {isLoginError && (
            <Alert status='error'>
              <AlertIcon />
              Credentials could not be verified. Please try again.
            </Alert>
          )}
          <Divider />
          <Box mt={2} p={4} bg='gray.800' borderRadius='md'>
            <Heading size='md' mb={2} textAlign='center'>
              U.S. Government Information System User Agreement
            </Heading>
            <Text mb={2}>
              You are accessing a U.S. Government (USG) Information System (IS)
              that is provided for USG-authorized use only. By using this IS
              (which includes any device attached to this IS), you consent to
              the following conditions:
            </Text>
            <UnorderedList>
              <ListItem>
                <Text>
                  The USG routinely intercepts and monitors communications on
                  this IS for purposes including, but not limited to,
                  penetration testing, COMSEC monitoring, network operations and
                  defense, personnel misconduct (PM), law enforcement (LE), and
                  counterintelligence (CI) investigations. At any time, the USG
                  may inspect and seize data stored on this IS.
                </Text>
              </ListItem>
              <ListItem>
                <Text>
                  Communications using, or data stored on, this IS are not
                  private, are subject to routine monitoring, interception, and
                  search, and may be disclosed or used for any USG-authorized
                  purpose.
                </Text>
              </ListItem>
              <ListItem>
                <Text>
                  This IS includes security measures (e.g., authentication and
                  access controls) to protect USG interests-not for your
                  personal benefit or privacy.
                </Text>
              </ListItem>
              <ListItem>
                <Text>
                  Notwithstanding the above, using this IS does not constitute
                  consent to PM, LE or CI investigative searching or monitoring
                  of the content of privileged communications, or work product,
                  related to personal representation or services by attorneys,
                  psychotherapists, or clergy, and their assistants. Such
                  communications and work product are private and confidential.
                  See User Agreement for details.
                </Text>
              </ListItem>
            </UnorderedList>
          </Box>
          <Divider />
          <Flex justifyContent='center'>
            <form
              onSubmit={handleSubmit(handleLogin as SubmitHandler<LoginProps>)}
            >
              <Stack spacing={4} width='25em'>
                <FormControl isInvalid={errors.username}>
                  <InputGroup>
                    <InputLeftElement>
                      <AccountIcon />
                    </InputLeftElement>
                    <Input
                      type='text'
                      placeholder='Username'
                      {...register('username', {
                        required: true,
                      })}
                      isDisabled={isLoginLoading}
                    />
                  </InputGroup>
                  <FormErrorMessage>
                    {errors.username && 'Username is required'}
                  </FormErrorMessage>
                </FormControl>
                <FormControl isInvalid={errors.password}>
                  <InputGroup>
                    <InputLeftElement>
                      <LockIcon />
                    </InputLeftElement>
                    <Input
                      placeholder='Password'
                      type='password'
                      {...register('password', {
                        required: true,
                      })}
                      isDisabled={isLoginLoading}
                    />
                  </InputGroup>

                  <FormErrorMessage>
                    {errors.password && 'Password is required'}
                  </FormErrorMessage>
                </FormControl>
                <FormControl textAlign='center'>
                  <Checkbox
                    name='accept'
                    isChecked={accepted}
                    onChange={handleChange}
                  >
                    Accept User Agreement
                  </Checkbox>
                </FormControl>
                <Divider />
                {isLoginLoading ? (
                  <Flex alignItems='center' justifyContent='center'>
                    <Spinner mr={2} />
                    <Text>Verifying credentials...</Text>
                  </Flex>
                ) : (
                  <Button
                    mt={2}
                    width='100%'
                    isDisabled={!accepted}
                    title={
                      accepted
                        ? undefined
                        : 'You must accept the User Agreement to log in'
                    }
                    isLoading={isSubmitting}
                    colorScheme='blue'
                    type='submit'
                    shadow='md'
                  >
                    Login
                  </Button>
                )}
              </Stack>
            </form>
          </Flex>
        </Stack>
      </Box>
    </Flex>
  )
}

export default LoginPage
