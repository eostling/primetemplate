import { screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { renderWithProviders } from '../../testUtils'
import LoginPage from './LoginPage'

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => jest.fn(),
}))

describe('<LoginPage/>', () => {
  const setup = () => {
    renderWithProviders(<LoginPage />)
  }

  test('should render without error', () => {
    setup()
    expect(
      screen.getByText(/Base Defense Operations Center/)
    ).toBeInTheDocument()
  })

  test('login button should be disabled on load', () => {
    setup()
    expect(screen.getByRole('button')).toBeDisabled()
  })

  test('login button enabled when user agreement is checked', () => {
    setup()
    const checkbox = screen.getByRole('checkbox')
    userEvent.click(checkbox)
    expect(screen.getByRole('button')).toBeEnabled()
  })
})
