import Router from './Router'
import { screen } from '@testing-library/react'
import { renderWithProviders } from '../../testUtils'

describe('<Router />', () => {
  const setup = () => {
    renderWithProviders(<Router />)
  }

  test('Should route without error', () => {
    setup()
    expect(screen.getByText('Login')).toBeDefined()
  })
})
