import React, { ReactElement } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import AlarmPage from '../AlarmPage/AlarmPage'
import LoginPage from '../LoginPage/LoginPage'
import PrivateRoute from './PrivateRoute/PrivateRoute'

const Router = (): ReactElement => {
  return (
    <BrowserRouter>
      <>
        <Routes>
          <Route path='/login' element={<LoginPage />} />
          <>
            <Route path='/' element={<PrivateRoute />}>
              <Route path='/' element={<AlarmPage />} />
            </Route>
          </>
        </Routes>
      </>
    </BrowserRouter>
  )
}

export default Router
