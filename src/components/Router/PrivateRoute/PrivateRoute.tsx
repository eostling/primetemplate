import React, { ReactElement } from 'react'
import { Navigate, Outlet } from 'react-router-dom'
import { useAuth } from '../../../hooks/useAuth/useAuth'

const PrivateRoute = (): ReactElement => {
  const { isLoggedIn } = useAuth()
  return isLoggedIn ? <Outlet /> : <Navigate to='/login' />
}

export default PrivateRoute
