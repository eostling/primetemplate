import React, { ReactElement, useEffect } from 'react'
import {
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
} from '@chakra-ui/react'
import useCountdown from '../../hooks/useCountdown/useCountdown'
import { useAuth } from '../../hooks/useAuth/useAuth'

const IdleTimer = (): ReactElement => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { handleLogout } = useAuth()
  const warnMS = process.env.REACT_APP_SESSION_WARN_MS as unknown as number
  const warnSeconds = warnMS / 1000
  const timer = useCountdown(warnSeconds)

  useEffect(() => {
    if (timer === 0) {
      handleLogout()
    }

    if (timer < 60) {
      onOpen()
    }
    return () => {
      onClose()
    }
  }, [timer, handleLogout, onClose, onOpen])

  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered data-testid='data'>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader color='white'>
          Session Expiring in {timer} Seconds
        </ModalHeader>
        <ModalBody fontWeight='bold' align='center' color='white'>
          <Text lineHeight='200%'>
            Interact with this page to avoid being logged out.
          </Text>
        </ModalBody>
      </ModalContent>
    </Modal>
  )
}

export default IdleTimer
