import { render, screen } from '@testing-library/react'
import ErrorFallback from './ErrorFallback'

describe('<ErrorFallback />', () => {
  const error = new Error('Oops! Looks like we have a problem')
  const setup = () => {
    render(<ErrorFallback error={error} />)
  }

  test('Should render without error', () => {
    setup()
    expect(screen.getByText('Oops! Looks like we have a problem')).toBeDefined()
  })
})
