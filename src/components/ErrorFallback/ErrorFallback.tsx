import React, { ReactElement } from 'react'
import { Flex, Code, Heading } from '@chakra-ui/react'

interface ErrorFallbackProps {
  componentStack?: {}
  error: Error
}

const ErrorFallback = ({
  componentStack,
  error,
}: ErrorFallbackProps): ReactElement => {
  return (
    <Flex
      height='100vh'
      width='100vw'
      bg='gray.900'
      justifyContent='center'
      alignItems='center'
      flexDirection='column'
    >
      <Heading mb={4} as='h1'>
        Oops! Looks like we have a problem
      </Heading>
      <Code colorScheme='bgColor' children={error.toString()} />
      {componentStack && (
        <Code
          colorScheme='bgColor'
          children={`Additional info: ${componentStack}`}
        />
      )}
    </Flex>
  )
}

export default ErrorFallback
