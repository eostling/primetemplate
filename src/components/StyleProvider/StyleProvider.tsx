import React, { ReactElement } from 'react'
import { ChakraProvider } from '@chakra-ui/react'
import theme from '../../theme'

interface StyleProviderProps {
  children: ReactElement
}

const StyleProvider = ({ children }: StyleProviderProps): ReactElement => {
  return <ChakraProvider theme={theme}>{children}</ChakraProvider>
}

export default StyleProvider
