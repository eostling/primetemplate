import React, { ReactElement } from 'react'
import QueueContainer from './QueueContainer/QueueContainer'
import Appbar from '../Appbar/Appbar'
import { ErrorBoundary } from 'react-error-boundary'
import { Flex, Box } from '@chakra-ui/react'
import Map from './Map/Map'
import ErrorFallback from '../ErrorFallback/ErrorFallback'
import GlobalWatcher from '../GlobalWatcher/GlobalWatcher'

const AlarmPage = (): ReactElement => {
  return (
    <Flex
      flexDirection='column'
      height='100vh'
      width='100vw'
      backgroundColor='gray.900'
    >
      <GlobalWatcher />
      <Appbar />
      <Flex flexGrow={1} position='relative'>
        <Flex
          flexGrow={1}
          minHeight='100%'
          maxHeight='1em'
          overflowX='auto'
          overflowY='hidden'
        >
          <ErrorBoundary FallbackComponent={ErrorFallback}>
            <Flex w='100%'>
              <Box flexGrow={1}>
                <Map />
              </Box>
              <QueueContainer />
            </Flex>
          </ErrorBoundary>
        </Flex>
      </Flex>
    </Flex>
  )
}

export default AlarmPage
