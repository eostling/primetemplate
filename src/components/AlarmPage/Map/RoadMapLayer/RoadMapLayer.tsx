import React, { ReactElement } from 'react'
import { Layer, Source } from 'react-map-gl'
import getThemeColor from '../../../../utils/getThemeColor/getThemeColor'
import * as GeoJSON from 'geojson'
import { AnyLayout } from 'maplibre-gl'

interface RoadMapLayerProps {
  roads:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
  minZoom: number
}

const RoadMapLayer = ({
  roads,
  minZoom,
}: RoadMapLayerProps): ReactElement | null => {
  const gray400 = getThemeColor('gray.400')

  const fillLayerConfig = {
    id: 'roads-fill',
    type: 'fill',
    minzoom: 5,
    paint: {
      'fill-color': gray400,
      'fill-opacity': 0.2,
    },
  }

  const labelLayerConfig = {
    id: 'roads-label',
    type: 'symbol',
    minzoom: minZoom,
    layout: {
      'text-field': ['format', ['upcase', ['get', 'SDSFEATURENAME']]],
      'text-variable-anchor': ['center'],
      'text-radial-offset': -100,
      'text-size': 10,
    },
    paint: {
      'text-color': 'white',
      'text-halo-color': 'rgba(0,0,0,0.7)',
      'text-halo-width': 0.8,
      'text-halo-blur': 0.5,
    },
  }

  return roads ? (
    <Source id='roads' type='geojson' data={roads}>
      <Layer
        id={fillLayerConfig.id}
        type='fill'
        minzoom={fillLayerConfig.minzoom}
        paint={fillLayerConfig.paint}
      />
      <Layer
        id={labelLayerConfig.id}
        type='symbol'
        minzoom={labelLayerConfig.minzoom}
        layout={labelLayerConfig.layout as AnyLayout}
        paint={labelLayerConfig.paint}
      />
    </Source>
  ) : null
}

export default RoadMapLayer
