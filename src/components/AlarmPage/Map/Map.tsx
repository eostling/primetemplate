import { useState, useContext } from 'react'
import ReactMapGL from 'react-map-gl'
import { Flex, Stack } from '@chakra-ui/react'

import MapNavControl from './MapNavControl/MapNavControl'
import MapStyleControl from './MapStyleControl/MapStyleControl'
import BuildingPopup from './BuildingPopup/BuildingPopup'
import MarkerPopup from './MarkerPopup/MarkerPopup'
import * as GeoJSON from 'geojson'
import genBlueForceGeoJson from '../../../utils/genBlueForceGeoJson/genBlueForceGeoJson'
import BlueForceMapLayer from './BlueForceMapLayer/BlueForceMapLayer'
import RoadMapLayer from './RoadMapLayer/RoadMapLayer'
import MarkerMapLayer from './MarkerMaplayer/MarkerMapLayer'
import { geojsonByState } from '../../../utils/geoJsonByState/geojsonByState'
import AreaMapLayer from './AreaMapLayer/AreaMapLayer'
import BuildingMapLayer from './BuildingMapLayer/BuildingMapLayer'
import axios from 'axios'
import { DEFAULT_MAP_STYLE_KEY, maxZoom, minZoom } from '../../../constants'
import getMapStyles from '../../../config/mapStyles'
import { allMapSettings } from '../../../config/mapSettings'
import { Alarm } from '../../../types'
import MapController from './MapController/MapController'
import { useSensors } from '../../../hooks/useSensors/useSensors'
import { useAlarms } from '../../../hooks/useAlarms/useAlarms'
import { SocketContext } from '../../../context/socket'

const baseViewPort = {
  width: '75vw',
  height: '100vh',
  latitude: 35.348,
  longitude: -77.96,
  zoom: 13,
  bearing: 0,
  pitch: 1,
  center: [35.348, -77.96],
  activeFloor: '',
  activeBuilding: '',
  floorplan: { type: 'FeatureCollection', features: [] },
}

const mappingStyles: any = getMapStyles(
  process.env.REACT_APP_MAPBOX_SPRITES_URL,
  process.env.REACT_APP_MAPBOX_GLYPHS_URL
)

const Map = () => {
  const [viewport, setViewPort] = useState(baseViewPort)
  const [styleKey, setStyleKey] = useState(DEFAULT_MAP_STYLE_KEY)
  const [showPopup, setShowPopup] = useState(false)
  const [popupData, setPopupData] = useState({
    type: '',
    props: {},
    coords: [],
  })
  const socket = useContext(SocketContext)

  const { area, buildings, roads, maxBounds } = allMapSettings.sjafb

  const { allAlarms } = useAlarms()
  const sensors = useSensors()

  const emptyFloor = { type: 'FeatureCollection', features: [] }

  const [connectedToServer, setConnectedToServer] = useState<boolean>(true)

  const {
    maxLatitude = 85,
    minLatitude = -85,
    maxLongitude = 180,
    minLongitude = -180,
  } = maxBounds

  const getBlueForceLocations = async () => {
    const { data } = await axios.get('/api/blueforce/location')
    return data
  }

  const blueForceDevices = useState(getBlueForceLocations)
  const blueForceGeoJson = blueForceDevices.filter(
    ({ sensorName, longitude, latitude }: any) => ({
      sensorName,
      longitude,
      latitude,
    })
  )

  const blueForce = genBlueForceGeoJson(blueForceGeoJson)

  const getFloor = async (facilityID: any, floor: any) => {
    const geoJsonUrl = `/api/floorplan/?facility=${facilityID}&floor=${floor}`
    try {
      const { data } = await axios.get(geoJsonUrl)

      setViewPort((prevState) => {
        return { ...prevState, activeFloor: data.activeFloor }
      })

      return data
    } catch (err) {
      return err
    }
  }

  const onViewportChange = (nextViewport: any) => {
    const adjustedViewport = nextViewport
    if (nextViewport.longitude < minLongitude) {
      adjustedViewport.longitude = minLongitude
    } else if (nextViewport.longitude > maxLongitude) {
      adjustedViewport.longitude = maxLongitude
    }
    if (nextViewport.latitude < minLatitude) {
      adjustedViewport.latitude = minLatitude
    } else if (nextViewport.latitude > maxLatitude) {
      adjustedViewport.latitude = maxLatitude
    }

    setViewPort((prevState) => {
      return { ...prevState, ...adjustedViewport, transitionDuration: 0 }
    })
    setShowPopup(adjustedViewport.zoom > 15 ? showPopup : false)
  }

  const handleClosePopup = () => {
    setShowPopup(false)
  }

  const onClickMapHandler = (e: any) => {
    const isFeature =
      e !== null &&
      e !== undefined &&
      Object.prototype.hasOwnProperty.call(e, 'features') &&
      Array.isArray(e.features) &&
      e.features.length

    if (isFeature) {
      const layerType = e.features[0].source.split('-')[0]

      if (layerType === 'markers') {
        const marker = e.features[0]
        const coords = marker.geometry.coordinates.slice()
        const props = allAlarms.find(
          (alarm: Alarm) => alarm.alarmID === marker.properties.alarmID
        )
        //@ts-ignore
        setPopupData({ type: layerType, coords, props })
        setShowPopup(true)
      } else if (layerType === 'buildings') {
        const building = e.features[0]
        const coords = e.lngLat
        const props = building.properties

        setPopupData({
          type: layerType,
          coords,
          props,
        })

        setShowPopup(true)
      }
    } else {
      setShowPopup(false)
    }
  }

  const mapController = new MapController()

  let interactiveLayerIds = [
    'point-SECURE',
    'point-UNKNOWN',
    'point-ALARM',
    'point-ACCESS',
    'point-ACK_UNKNOWN',
    'point-ACKNOWLEDGED',
  ]

  if (buildings) {
    interactiveLayerIds.push('buildings-fill')
  }

  if (roads) {
    interactiveLayerIds.push('roads-fill')
  }

  let [blueForceToggle, setBlueForceToggle] = useState(false)
  const handleBlueForceLayerToggle = () => {
    setBlueForceToggle(!blueForceToggle)
  }

  const updateBuildingPopup = (data: any) => {
    setViewPort((prevState: any) => {
      return {
        ...prevState,
        activeFloor: data.activeFloor,
        activeBuilding: data.building,
      }
    })

    if (data) {
      getFloor(data.building, data.activeFloor).then((floorplan) => {
        setViewPort((prevState: any) => {
          return { ...prevState, floorplan: floorplan }
        })
      })
    } else {
      setViewPort((prevState: any) => {
        return { ...prevState, floorplan: emptyFloor }
      })
    }
  }

  const handleStyle = (key: any) => {
    setStyleKey(key)
  }

  const zoomIn = () => {
    if (viewport.zoom < maxZoom && viewport.zoom >= minZoom) {
      setViewPort((prevState) => {
        return { ...prevState, zoom: prevState.zoom + 1 }
      })
    }
  }

  const zoomOut = () => {
    if (viewport.zoom > minZoom && viewport.zoom <= maxZoom) {
      setViewPort((prevState) => {
        return { ...prevState, zoom: prevState.zoom - 1 }
      })
    }
  }

  const resetBearing = (): void => {
    const isDefault = viewport.pitch === 0 && viewport.bearing === 0
    viewport.pitch = isDefault ? 60 : 0
    setViewPort((prevState) => {
      return { ...prevState, bearing: 0 }
    })
  }

  const flyTo = (lat: number, lon: number) => {
    setViewPort((prevState) => {
      return { ...prevState, latitude: lat, longitude: lon }
    })
  }

  socket.on('disconnect', () => {
    setConnectedToServer(false)
  })

  return (
    <ReactMapGL
      controller={mapController}
      mapStyle={mappingStyles[styleKey]}
      maxZoom={maxZoom}
      minZoom={minZoom}
      onViewportChange={onViewportChange}
      onClick={onClickMapHandler}
      interactiveLayerIds={interactiveLayerIds}
      {...viewport}
    >
      <Stack p={4}>
        <Flex>
          <MapStyleControl
            styleKey={styleKey}
            onStyleChange={handleStyle}
            onBlueForceLayerToggle={handleBlueForceLayerToggle}
            blueForceToggle={blueForceToggle}
          />
        </Flex>
        <Flex>
          <MapNavControl
            pitch={viewport.pitch}
            bearing={viewport.bearing}
            onZoomIn={zoomIn}
            onZoomOut={zoomOut}
            onResetBearing={resetBearing}
            flyTo={flyTo}
          />
        </Flex>
      </Stack>
      <BuildingPopup
        data={popupData}
        show={showPopup && popupData.type === 'buildings'}
        handleClose={handleClosePopup}
        viewport={viewport}
        setFloorFilter={updateBuildingPopup}
      />
      <AreaMapLayer
        area={
          area as
            | GeoJSON.Feature<GeoJSON.Geometry>
            | GeoJSON.FeatureCollection<GeoJSON.Geometry>
        }
      />
      <BuildingMapLayer
        buildings={
          buildings as
            | GeoJSON.FeatureCollection<GeoJSON.Geometry>
            | GeoJSON.Feature<GeoJSON.Geometry>
        }
        floorplan={viewport.floorplan}
        minZoom={15}
      />
      <MarkerPopup
        data={popupData}
        show={showPopup && popupData.type === 'markers'}
        handleClose={handleClosePopup}
        alarms={allAlarms}
      />
      <RoadMapLayer
        roads={
          roads as unknown as
            | GeoJSON.FeatureCollection<GeoJSON.Geometry>
            | GeoJSON.Feature<GeoJSON.Geometry>
        }
        minZoom={5}
      />
      {connectedToServer && (
        <MarkerMapLayer
          markersSECURE={geojsonByState('SECURE', sensors, allAlarms) as any}
          markersACCESS={geojsonByState('ACCESS', sensors, allAlarms) as any}
          markersACK_UNKNOWN={
            geojsonByState('ACK_UNKNOWN', sensors, allAlarms) as any
          }
          markersUNKNOWN={geojsonByState('UNKNOWN', sensors, allAlarms) as any}
          markersACKNOWLEDGED={
            geojsonByState('ACKNOWLEDGED', sensors, allAlarms) as any
          }
          markersALARM={geojsonByState('ALARM', sensors, allAlarms) as any}
          minZoom={0}
        />
      )}
      {blueForce && blueForceToggle && (
        <BlueForceMapLayer blueForceDevices={blueForceDevices} minZoom={5} />
      )}
    </ReactMapGL>
  )
}

export default Map
