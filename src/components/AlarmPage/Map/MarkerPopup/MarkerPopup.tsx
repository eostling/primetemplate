import React, { ReactElement } from 'react'
import { Popup } from 'react-map-gl'
import {
  Flex,
  Heading,
  IconButton,
  List,
  ListItem,
  ListIcon,
  Stack,
  Text,
} from '@chakra-ui/react'

import { CloseIcon } from '../../../../utils/generateIcons'
import labelify from '../../../../utils/labelify/labelify'
import groupBy from '../../../../utils/groupBy/groupBy'
import AlarmStateIcon from '../../QueueContainer/Alarms/AlarmCard/AlarmStateIcon/AlarmStateIcon'

interface MarkerPopupProps {
  data: {
    coords: number[]
    props: any
  }
  show: boolean
  handleClose: VoidFunction
  alarms: any
}

const MarkerPopup = ({
  data,
  show,
  handleClose,
  alarms,
}: MarkerPopupProps): ReactElement | null => {
  const { coords, props } = data

  let sensors = []
  if (props) {
    const { lat, lon } = props
    sensors = alarms.filter((alarm) => alarm.lat === lat && alarm.lon === lon)
  }

  const sensorsByFloor = groupBy(sensors, 'floor')

  if (show && sensors.length === 0) {
    handleClose()
  }

  const getSensorsOnFloor = (floorNum, sensorsOnFloor): ReactElement => {
    return (
      <>
        <Heading size='sm'>Floor {floorNum}</Heading>
        <List>
          {sensorsOnFloor.map(
            ({ additionalAlarmDetails, alarmID, label, state }) => {
              const hasBatteryPercentage =
                Object.prototype.hasOwnProperty.call(
                  additionalAlarmDetails,
                  'batteryPercentage'
                ) &&
                additionalAlarmDetails.batteryPercentage !== null &&
                additionalAlarmDetails.batteryPercentage !== undefined
              const batteryLabel = hasBatteryPercentage
                ? ` (${additionalAlarmDetails.batteryPercentage}%)`
                : ''
              return (
                <ListItem
                  key={alarmID}
                  color={`states.${state}`}
                  d='flex'
                  alignItems='center'
                >
                  <ListIcon as={AlarmStateIcon} state={state} />
                  <Text ml={1}>{`${labelify(
                    label
                  )} ${batteryLabel} - ${state}`}</Text>
                </ListItem>
              )
            }
          )}
        </List>
      </>
    )
  }

  const sensorsList: ReactElement[] = []
  Object.keys(sensorsByFloor).forEach((floorNum) => {
    sensorsList.push(getSensorsOnFloor(floorNum, sensorsByFloor[floorNum]))
  })

  return show && sensors.length > 0 ? (
    <Popup
      latitude={coords[1]}
      longitude={coords[0]}
      closeButton={false}
      closeOnClick={false}
      anchor='bottom'
      //@ts-ignore
      style={{ backgroundColor: 'gray.500' }}
      captureScroll
    >
      <Stack spacing={1}>
        <Flex align='center' justify='space-between'>
          <Heading size='md'>{labelify(props.sensorName)}</Heading>
          <IconButton
            icon={<CloseIcon />}
            variant='link'
            size='md'
            aria-label='close'
            ml={1}
            onClick={handleClose}
          />
        </Flex>
        <List>
          {sensors.map(
            ({ alarmID, alarmType, state, additionalAlarmDetails }) => {
              const hasBatteryPercentage =
                Object.prototype.hasOwnProperty.call(
                  additionalAlarmDetails,
                  'batteryPercentage'
                ) &&
                //@ts-ignore
                additionalAlarmDetails.batteryPercentage !== null &&
                //@ts-ignore
                additionalAlarmDetails.batteryPercentage !== undefined

              const batteryLabel = hasBatteryPercentage
                ? //@ts-ignore
                  ` (${additionalAlarmDetails.batteryPercentage}%)`
                : ''
              return (
                <ListItem
                  key={alarmID}
                  color={`states.${state}`}
                  d='flex'
                  alignItems='center'
                >
                  <AlarmStateIcon state={state} />
                  <Text ml={1}>
                    {`${labelify(alarmType)}${batteryLabel} - ${state}`}
                  </Text>
                </ListItem>
              )
            }
          )}
        </List>
      </Stack>
    </Popup>
  ) : null
}

export default MarkerPopup
