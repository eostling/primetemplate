import React, { ReactElement } from 'react'
import { Layer, Source } from 'react-map-gl'
import usePulseAnimation from '../../../../hooks/usePulseAnimation.ts/usePulseAnimation'
import getThemeColor from '../../../../utils/getThemeColor/getThemeColor'
import * as GeoJSON from 'geojson'

interface MarkerMapLayerProps {
  markersSECURE:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
  markersACCESS:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
  markersACK_UNKNOWN:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
  markersUNKNOWN:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
  markersACKNOWLEDGED:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
  markersALARM:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
  minZoom: number
}

const MarkerMapLayer = ({
  markersSECURE,
  markersACCESS,
  markersACK_UNKNOWN,
  markersUNKNOWN,
  markersACKNOWLEDGED,
  markersALARM,
  minZoom,
}: MarkerMapLayerProps): ReactElement => {
  const { radius: pulseRadius, opacity: pulseOpacity } = usePulseAnimation()

  const genPointLayerConfig = (state: string) => ({
    id: `point-${state}`,
    type: 'circle',
    paint: {
      'circle-color': getThemeColor(`states.${state}`),
      'circle-radius': 5,
    },
    minzoom: minZoom,
  })

  const genPulseLayerConfig = (state: string) => ({
    id: `point-${state}-pulse`,
    type: 'circle',
    paint: {
      'circle-color': getThemeColor(`states.${state}`),
      'circle-radius': pulseRadius,
      'circle-opacity': pulseOpacity,
    },
    minzoom: minZoom,
  })

  const securePointLayerConfig = genPointLayerConfig('SECURE')
  const accessPointLayerConfig = genPointLayerConfig('ACCESS')
  const ackUnknownPointLayerConfig = genPointLayerConfig('ACK_UNKNOWN')
  const ackPointLayerConfig = genPointLayerConfig('ACKNOWLEDGED')
  const unknownPulseLayerConfig = genPulseLayerConfig('UNKNOWN')
  const unknownPointLayerConfig = genPointLayerConfig('UNKNOWN')
  const alarmPulseLayerConfig = genPulseLayerConfig('ALARM')
  const alarmPointLayerConfig = genPointLayerConfig('ALARM')

  return (
    <>
      <Source id='markers-SECURE' type='geojson' data={markersSECURE}>
        <Layer
          id={securePointLayerConfig.id}
          type='circle'
          paint={securePointLayerConfig.paint}
          minzoom={securePointLayerConfig.minzoom}
        />
      </Source>
      <Source id='markers-ACCESS' type='geojson' data={markersACCESS}>
        <Layer
          id={accessPointLayerConfig.id}
          type='circle'
          paint={accessPointLayerConfig.paint}
          minzoom={accessPointLayerConfig.minzoom}
        />
      </Source>
      <Source id='markers-ACK_UNKNOWN' type='geojson' data={markersACK_UNKNOWN}>
        <Layer
          id={ackUnknownPointLayerConfig.id}
          type='circle'
          paint={ackUnknownPointLayerConfig.paint}
          minzoom={ackUnknownPointLayerConfig.minzoom}
        />
      </Source>
      <Source
        id='markers-ACKNOWLEDGED'
        type='geojson'
        data={markersACKNOWLEDGED}
      >
        <Layer
          id={ackPointLayerConfig.id}
          type='circle'
          paint={ackPointLayerConfig.paint}
          minzoom={ackPointLayerConfig.minzoom}
        />
      </Source>
      <Source id='markers-UNKNOWN' type='geojson' data={markersUNKNOWN}>
        <Layer
          id={unknownPulseLayerConfig.id}
          type='circle'
          paint={unknownPulseLayerConfig.paint}
          minzoom={unknownPulseLayerConfig.minzoom}
        />
        <Layer
          id={unknownPointLayerConfig.id}
          type='circle'
          paint={unknownPointLayerConfig.paint}
          minzoom={unknownPointLayerConfig.minzoom}
        />
      </Source>
      <Source id='markers-ALARM' type='geojson' data={markersALARM}>
        <Layer
          id={alarmPulseLayerConfig.id}
          type='circle'
          paint={alarmPulseLayerConfig.paint}
          minzoom={alarmPulseLayerConfig.minzoom}
        />
        <Layer
          id={alarmPointLayerConfig.id}
          type='circle'
          paint={alarmPointLayerConfig.paint}
          minzoom={alarmPointLayerConfig.minzoom}
        />
      </Source>
    </>
  )
}

export default MarkerMapLayer
