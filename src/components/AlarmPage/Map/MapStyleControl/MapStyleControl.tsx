import React, { ReactElement } from 'react'
import { Stack } from '@chakra-ui/react'
import {
  MapIcon,
  PhoneIcon,
  SatelliteIcon,
} from '../../../../utils/generateIcons'
import IconButtonWithTooltip from '../../../IconButtonWithTooltip/IconButtonWithTooltip'

interface MapStyleControlProps {
  styleKey: string
  onStyleChange: (title: string) => void
  onBlueForceLayerToggle: () => void
  blueForceToggle: boolean
}

const MapStyleControl = ({
  styleKey,
  onStyleChange,
  onBlueForceLayerToggle,
  blueForceToggle,
}: MapStyleControlProps): ReactElement => {
  return (
    <Stack spacing={2} direction='column'>
      <Stack spacing={2} direction='row'>
        <IconButtonWithTooltip
          icon={<SatelliteIcon />}
          label='Satellite View'
          placement='right'
          onClick={() => onStyleChange('satellite')}
          size='sm'
          ariaLabel='satellite-view'
          variant={
            styleKey === 'satellite'
              ? 'map-control-active'
              : 'map-control-inactive'
          }
        />
        <IconButtonWithTooltip
          icon={<MapIcon />}
          label='Map View'
          placement='right'
          colorScheme={styleKey === 'dark' ? 'blue' : 'gray'}
          onClick={() => onStyleChange('dark')}
          size='sm'
          ariaLabel='map-view'
          variant={
            styleKey === 'dark' ? 'map-control-active' : 'map-control-inactive'
          }
        />
      </Stack>
      <Stack spacing={2} direction='row'>
        <IconButtonWithTooltip
          icon={<PhoneIcon />}
          label='Blue Force Layer Toggle'
          placement='right'
          onClick={() => onBlueForceLayerToggle()}
          size='sm'
          ariaLabel='blue-force-toggle'
          variant={
            blueForceToggle && styleKey
              ? 'map-control-active'
              : 'map-control-inactive'
          }
        />
      </Stack>
    </Stack>
  )
}

export default MapStyleControl
