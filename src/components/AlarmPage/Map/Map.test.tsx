import { BrowserRouter } from 'react-router-dom'
import { renderWithProviders } from '../../../testUtils'
import Map from './Map'

jest.mock('../../../hooks/useAuth/useAuth.ts', () => ({
  useAuth: () => ({ user: { username: 'brain', permissions: ['admin'] } }),
}))

describe('<Map />', () => {
  const setup = () => {
    renderWithProviders(
      <BrowserRouter>
        <Map />
      </BrowserRouter>
    )
  }

  test('should render without error', () => {
    setup()
  })
})
