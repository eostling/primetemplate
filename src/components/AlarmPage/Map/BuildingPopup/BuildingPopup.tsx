import React, { ReactElement } from 'react'
import { Popup } from 'react-map-gl'
import {
  ButtonGroup,
  Button,
  Flex,
  Heading,
  IconButton,
  Stack,
  Text,
} from '@chakra-ui/react'
import { CloseIcon } from '../../../../utils/generateIcons'
import { useFacilities } from '../../../../hooks/useFacilities/useFacilities'

interface BuildingPopupProps {
  data: any
  show: boolean
  handleClose: VoidFunction
  viewport: any
  setFloorFilter: any
}

const BuildingPopup = ({
  data,
  show,
  handleClose,
  viewport,
  setFloorFilter,
}: BuildingPopupProps): ReactElement => {
  const { coords, props } = data

  const { allFacilities } = useFacilities()

  const buildingNumber = props.BUILDINGNUMBER
    ? props.BUILDINGNUMBER
    : 'No Building Number was Supplied'

  const hasAddress = props.BUILDINGADDRESS
    ? props.BUILDINGADDRESS
    : 'No building address was supplied'

  const fac = allFacilities.filter(
    (fac) => fac.facilityID === buildingNumber
  )[0]

  const floors = fac ? Object.keys(fac.cadFilenames) : []

  const floorFilter = {
    building: viewport.activeBuilding,
    floor: viewport.activeFloor,
  }

  const handleFloorClick = (floor: any) => {
    const floorFilter = { building: props.BUILDINGNUMBER, activeFloor: floor }
    setFloorFilter(floorFilter)
  }

  const isNone = !floorFilter.floor

  return (
    <>
      {show && (
        <Popup
          latitude={coords[1]}
          longitude={coords[0]}
          closeButton={false}
          anchor='bottom'
        >
          <Stack spacing={1}>
            <Flex align='center' justify='space-between'>
              <Heading size='md'>{props.SDSFEATURENAME}</Heading>
              <IconButton
                icon={<CloseIcon />}
                variant='link'
                size='md'
                ml={1}
                onClick={handleClose}
                aria-label='temp'
              />
            </Flex>
            <Text fontSize='xs' color='gray.400'>
              {`Bldg ${props.BUILDINGNUMBER}${
                hasAddress ? `, ${props.BUILDINGADDRESS}` : ''
              }`}
            </Text>
            {floors.length ? (
              <>
                <Heading size='xs' mb={1}>
                  Show Floor
                </Heading>
                <ButtonGroup spacing={1}>
                  {floors.map((floor: any) => {
                    const isActiveFloor =
                      floorFilter.building === props.BUILDINGNUMBER &&
                      floorFilter.floor === floor

                    return (
                      <Button
                        key={`floor-${floor}-btn`}
                        size='sm'
                        colorScheme={isActiveFloor ? 'blue' : undefined}
                        onClick={() => handleFloorClick(floor)}
                      >
                        {floor}
                      </Button>
                    )
                  })}
                  <Button
                    size='sm'
                    colorScheme={isNone ? 'blue' : undefined}
                    onClick={() => handleFloorClick(undefined)}
                  >
                    None
                  </Button>
                </ButtonGroup>
              </>
            ) : (
              <Text as='i' fontSize='sm'>
                No floor data available.
              </Text>
            )}
          </Stack>
        </Popup>
      )}
    </>
  )
}

export default BuildingPopup
