import React, { ReactElement } from 'react'
import { Layer, Source } from 'react-map-gl'
import getThemeColor from '../../../../utils/getThemeColor/getThemeColor'
import { AnyLayout, AnyPaint } from 'maplibre-gl'

interface BlueForceMapLayerProps {
  blueForceDevices: any
  minZoom: number
}

const BlueForceMapLayer = ({
  blueForceDevices,
  minZoom,
}: BlueForceMapLayerProps): ReactElement => {
  const blueForceColor = getThemeColor('blueForce.normal.fill')
  const blueForceBorderColor = getThemeColor('blueForce.normal.border')
  const distressedBlueForceColor = getThemeColor('blueForce.distressed.fill')
  const distressedBlueForceBorderColor = getThemeColor(
    'blueForce.distressed.border'
  )

  const pointLayerConfig = {
    id: 'point-blue-force-device',
    type: 'circle',
    minzoom: minZoom,
    paint: {
      'circle-color': [
        'case',
        ['boolean', ['get', 'distress'], false],
        distressedBlueForceColor,
        blueForceColor,
      ],
      'circle-radius': 5,
      'circle-stroke-color': [
        'case',
        ['boolean', ['get', 'distress'], false],
        distressedBlueForceBorderColor,
        blueForceBorderColor,
      ],
      'circle-stroke-width': 1,
    },
  }

  const pointAccuracyLayerConfig = {
    id: 'point-blue-force-device-accuracy',
    type: 'circle',
    minzoom: minZoom,
    paint: {
      'circle-color': blueForceColor,
      'circle-opacity': 0.2,
      'circle-radius': 20,
      'circle-stroke-color': blueForceBorderColor,
      'circle-stroke-opacity': 0.2,
      'circle-stroke-width': 1,
    },
  }

  const labelLayerConfig = {
    id: 'label-blue-force-device',
    type: 'symbol',
    minzoom: minZoom,
    layout: {
      'text-field': ['get', 'displayName'],
      'text-variable-anchor': ['bottom'],
      'text-radial-offset': 0.6,
      'text-size': 15,
      'text-justify': 'auto',
    },
    paint: {
      'text-color': 'white',
      'text-halo-color': 'rgba(0,0,0,.7)',
      'text-halo-width': 1.2,
      'text-halo-blur': 0.5,
    },
  }

  return (
    <Source
      id='markers-blue-force-device'
      type='geojson'
      data={blueForceDevices}
    >
      <Layer
        id={pointLayerConfig.id}
        type='circle'
        minzoom={pointLayerConfig.minzoom}
        paint={pointLayerConfig.paint as AnyPaint}
        beforeId='point-SECURE'
      />
      <Layer
        id={pointAccuracyLayerConfig.id}
        type='circle'
        minzoom={pointAccuracyLayerConfig.minzoom}
        paint={pointAccuracyLayerConfig.paint}
      />
      <Layer
        id={labelLayerConfig.id}
        type='symbol'
        minzoom={labelLayerConfig.minzoom}
        layout={labelLayerConfig.layout as AnyLayout}
        paint={labelLayerConfig.paint}
      />
    </Source>
  )
}

export default BlueForceMapLayer
