import React, { ReactElement } from 'react'
import { Layer, Source } from 'react-map-gl'
import * as GeoJSON from 'geojson'

interface AreaMapLayerProps {
  area:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
}

const AreaMapLayer = ({ area }: AreaMapLayerProps): ReactElement => {
  const lineLayerConfig = {
    id: 'base-boundary-line',
    type: 'line',
    paint: {
      'line-color': 'orange',
      'line-width': 5,
    },
  }
  const fillLayerConfig = {
    id: 'base-boundary-fill',
    maxzoom: 15,
    type: 'fill',
    paint: {},
  }

  return (
    <>
      {area ? (
        <Source id='base-boundary' type='geojson' data={area}>
          <Layer
            id={lineLayerConfig.id}
            type='line'
            paint={lineLayerConfig.paint}
          />
          <Layer
            id={fillLayerConfig.id}
            maxzoom={fillLayerConfig.maxzoom}
            type='fill'
            paint={fillLayerConfig.paint}
          />
        </Source>
      ) : (
        <></>
      )}
    </>
  )
}

export default AreaMapLayer
