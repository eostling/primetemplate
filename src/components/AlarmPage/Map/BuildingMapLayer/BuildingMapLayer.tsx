import React, { ReactElement } from 'react'
import { Layer, Source } from 'react-map-gl'
import getThemeColor from '../../../../utils/getThemeColor/getThemeColor'
import * as GeoJSON from 'geojson'
import { AnyLayout } from 'maplibre-gl'

interface BuildingMapLayerProps {
  buildings:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
  floorplan: any
  minZoom: number
}

const BuildingMapLayer = ({
  buildings,
  floorplan,
  minZoom,
}: BuildingMapLayerProps): ReactElement | null => {
  const blue400 = getThemeColor('blue.400')

  const lineLayerConfig = {
    id: 'buildings-line',
    type: 'line',
    minzoom: minZoom,
    paint: {
      'line-color': blue400,
    },
  }

  const fillLayerConfig = {
    id: 'buildings-fill',
    type: 'fill',
    minzoom: minZoom,
    paint: {
      'fill-color': blue400,
      'fill-opacity': 0.2,
    },
  }

  const labelLayerConfig = {
    id: 'buildings-label',
    type: 'symbol',
    minzoom: minZoom,
    layout: {
      'text-field': [
        'format',
        ['upcase', ['get', 'SDSFEATURENAME']],
        '\n(',
        ['upcase', ['get', 'BUILDINGNUMBER']],
        ')',
      ],
      'text-variable-anchor': ['bottom'],
      'text-radial-offset': -100,
      'text-size': 14,
    },
    paint: {
      'text-color': 'white',
      'text-halo-color': 'rgba(0,0,0,0.7)',
      'text-halo-width': 0.8,
      'text-halo-blur': 0.5,
    },
  }

  const floorplanLineLayerConfig = {
    id: 'floorplan-line',
    type: 'line',
    minzoom: minZoom,
    paint: {
      'line-color': blue400,
    },
  }

  const roomLayer = {
    id: 'room-label',
    type: 'symbol',
    minzoom: minZoom,
    layout: {
      'text-field': ['get', 'ROOMID'],
      'text-size': 10,
    },
    paint: {
      'text-color': 'white',
    },
  }

  return buildings ? (
    <>
      <Source id='buildings' type='geojson' data={buildings}>
        <Layer
          id={lineLayerConfig.id}
          type='line'
          minzoom={lineLayerConfig.minzoom}
          paint={lineLayerConfig.paint}
        />
        <Layer
          id={fillLayerConfig.id}
          type='fill'
          minzoom={fillLayerConfig.minzoom}
          paint={fillLayerConfig.paint}
        />
        <Layer
          id={labelLayerConfig.id}
          type='symbol'
          minzoom={labelLayerConfig.minzoom}
          layout={labelLayerConfig.layout as AnyLayout}
          paint={labelLayerConfig.paint}
        />
      </Source>
      <Source id='floor' type='geojson' data={floorplan}>
        <Layer
          id={floorplanLineLayerConfig.id}
          type='line'
          minzoom={floorplanLineLayerConfig.minzoom}
          paint={floorplanLineLayerConfig.paint}
        />
        <Layer
          id={roomLayer.id}
          type='symbol'
          minzoom={roomLayer.minzoom}
          layout={roomLayer.layout as AnyLayout}
          paint={roomLayer.paint}
        />
      </Source>
    </>
  ) : null
}

export default BuildingMapLayer
