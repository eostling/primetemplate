import { ButtonGroup, Stack, VStack } from '@chakra-ui/react'
import React, { ReactElement } from 'react'

import { allMapSettings } from '../../../../config/mapSettings'
import {
  CompassArrowIcon,
  MinusIcon,
  PlusIcon,
  TargetIcon,
} from '../../../../utils/generateIcons'
import IconButtonWithTooltip from '../../../IconButtonWithTooltip/IconButtonWithTooltip'

interface MapNavControlProps {
  pitch: number
  bearing: number
  onZoomIn: any
  onZoomOut: any
  onResetBearing: any
  flyTo: any
}

const MapNavControl = ({
  pitch,
  bearing,
  onZoomIn,
  onZoomOut,
  onResetBearing,
  flyTo,
}: MapNavControlProps): ReactElement => {
  const { locations } = allMapSettings.sjafb

  return (
    <Stack spacing={2}>
      <IconButtonWithTooltip
        icon={<CompassArrowIcon />}
        label={pitch === 0 ? 'Toggle Perspective' : 'Reset Orientation'}
        placement='right'
        onClick={onResetBearing}
        p={0}
        size='sm'
        style={{
          transform: `rotate(${-bearing}deg) rotateX(${pitch}deg)`,
        }}
        variant='map-control'
        ariaLabel='toggle-perspectives'
        color='black'
      />
      {locations.map((loc: any) => (
        <IconButtonWithTooltip
          icon={<TargetIcon />}
          key={loc.name}
          label={`Center on ${loc.name}`}
          placement='right'
          size='sm'
          variant='map-control'
          ariaLabel='center-on-target'
          onClick={() => flyTo(loc.latitude, loc.longitude)}
          color='black'
        />
      ))}
      <VStack>
        <ButtonGroup
          size='sm'
          isAttached
          variant='outline'
          style={{
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <IconButtonWithTooltip
            icon={<PlusIcon />}
            label='Zoom In'
            placement='right'
            onClick={onZoomIn}
            size='sm'
            ariaLabel='Zoom In'
            variant='map-control'
            style={{
              borderBottomLeftRadius: '0',
              borderBottomRightRadius: '0',
            }}
            color='black'
          />
          <IconButtonWithTooltip
            icon={<MinusIcon />}
            label='Zoom Out'
            placement='right'
            onClick={onZoomOut}
            size='sm'
            ariaLabel='Zoom Out'
            variant='map-control'
            style={{
              borderTopLeftRadius: '0',
              borderTopRightRadius: '0',
            }}
            color='black'
          />
        </ButtonGroup>
      </VStack>
    </Stack>
  )
}

export default MapNavControl
