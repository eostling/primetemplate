import { screen } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import { renderWithProviders } from '../../testUtils'
import AlarmPage from './AlarmPage'

jest.mock('../../hooks/useAuth/useAuth', () => ({
  useAuth: () => ({ user: { username: 'brain', permissions: [] } }),
}))

describe('<AlarmPage />', () => {
  const originalEnv = process.env

  beforeEach(() => {
    jest.resetModules()
    process.env = {
      ...originalEnv,
      REACT_APP_VERSION_NUMBER: '1.0.7',
      REACT_APP_NAME: 'PRIME',
    }
  })

  afterEach(() => {
    process.env = originalEnv
  })

  const setup = () => {
    renderWithProviders(
      <BrowserRouter>
        <AlarmPage />
      </BrowserRouter>
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('PRIME')).toBeDefined()
  })
})
