import { screen } from '@testing-library/react'
import { renderWithProviders } from '../../../../testUtils'
import SecureDialog from './SecureDialog'

describe('<SecureDialog />', () => {
  const setup = () => {
    renderWithProviders(
      <SecureDialog isOpen={true} onClose={jest.fn} alarms={[]} />
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('Secure Alarm')).toBeDefined()
  })
})
