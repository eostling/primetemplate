import React, { ChangeEvent, ReactElement, useRef, useState } from 'react'
import {
  Button,
  Divider,
  Flex,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Stack,
  Textarea,
  Box,
} from '@chakra-ui/react'

import { Alarm } from '../../../../types/index'
import { AlarmSecureIcon } from '../../../../utils/generateIcons'
import { useAlarms } from '../../../../hooks/useAlarms/useAlarms'
import { useCustomToast } from '../../../../hooks/useCustomToast/useCustomToast'

interface SecureDialogProps {
  isOpen: boolean
  onClose: VoidFunction
  alarms: Alarm[]
}

const SecureDialog = ({
  isOpen,
  onClose,
  alarms,
}: SecureDialogProps): ReactElement => {
  const [assessment, setAssessment] = useState<string>('Valid')
  const [remark, setRemark] = useState<string>('')
  const initialFocus = useRef<HTMLSelectElement>(null)
  const { handleAlarmSecure } = useAlarms()
  const toast = useCustomToast()

  const handleAssessment = (e: ChangeEvent<HTMLSelectElement>) => {
    const inputValue = e.target.value
    setAssessment(inputValue)
  }

  const handleRemark = (e: ChangeEvent<HTMLTextAreaElement>) => {
    const inputValue = e.target.value
    setRemark(inputValue)
  }

  const onSubmit = () => {
    handleAlarmSecure(
      { alarms, remark, assessment },
      {
        onSuccess: (response) => {
          const updatedAlarms = response.filter((res) => {
            return res.statusCode === 200
          })
          const alarms = updatedAlarms.length === 1 ? 'Alarm' : 'Alarms'
          const description = 'Secured '
            .concat(updatedAlarms.length)
            .concat(` ${alarms}`)

          toast({
            title: `${alarms} updated`,
            description,
            status: 'success',
          })
        },
        onSettled: () => {
          setAssessment('Valid')
          setRemark('')
          onClose()
        },
      }
    )
  }

  return (
    <>
      <Modal
        initialFocusRef={initialFocus}
        isOpen={isOpen}
        onClose={onClose}
        scrollBehavior='inside'
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader color='white'>
            {`Secure ${alarms.length > 1 ? 'Alarms' : 'Alarm'}`}
          </ModalHeader>
          <ModalCloseButton color='white' />
          <ModalBody>
            <Stack spacing={2}>
              {alarms.map((alarm) => {
                return (
                  <Flex key={alarm.alarmID} color='white'>
                    <Box p='1'>{alarm.label}</Box>
                  </Flex>
                )
              })}
            </Stack>
            <Divider my={2} />
            <Stack spacing={2}>
              <Select
                onChange={handleAssessment}
                value={assessment}
                bg='gray.700'
                ref={initialFocus}
              >
                <option value='Valid'>Valid</option>
                <option value='Nuisance'>Nuisance</option>
                <option value='False Positive'>False Positive</option>
              </Select>
              <Textarea
                value={remark}
                onChange={handleRemark}
                placeholder='Optional remark...'
                size='sm'
                color='white'
              />
            </Stack>
          </ModalBody>
          <ModalFooter>
            <Button variant='outline' mr={3} onClick={onClose}>
              Cancel
            </Button>
            <Button type='submit' colorScheme='green' onClick={onSubmit}>
              <AlarmSecureIcon />
              Secure
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default SecureDialog
