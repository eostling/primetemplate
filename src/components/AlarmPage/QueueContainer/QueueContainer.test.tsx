import { screen } from '@testing-library/react'
import { renderWithProviders } from '../../../testUtils'
import QueueContainer from './QueueContainer'

describe('<QueueContainer />', () => {
  const setup = () => {
    renderWithProviders(<QueueContainer />)
  }

  test('should render without error', () => {
    setup()
    const items = screen.getAllByText('Loading...')
    expect(items).toHaveLength(2)
  })
})
