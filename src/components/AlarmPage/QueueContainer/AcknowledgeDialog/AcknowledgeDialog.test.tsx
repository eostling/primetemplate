import { screen } from '@testing-library/react'
import { renderWithProviders } from '../../../../testUtils'
import AcknowledgeDialog from './AcknowledgeDialog'

describe('<AcknowledgeDialog />', () => {
  const setup = () => {
    renderWithProviders(
      <AcknowledgeDialog isOpen={true} onClose={jest.fn} alarms={[]} unknown />
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('Confirm Acknowledge')).toBeDefined()
  })
})
