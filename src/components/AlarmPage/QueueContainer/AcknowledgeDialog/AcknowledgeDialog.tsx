import React from 'react'
import {
  Box,
  Button,
  Divider,
  Flex,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
} from '@chakra-ui/react'
import { Alarm } from '../../../../types'
import { AlarmAcknowledgedIcon } from '../../../../utils/generateIcons'
import { useAlarms } from '../../../../hooks/useAlarms/useAlarms'
import { useCustomToast } from '../../../../hooks/useCustomToast/useCustomToast'

interface AcknowledgeDialogProps {
  isOpen: boolean
  onClose: VoidFunction
  alarms: Alarm[]
  unknown: boolean
}

const AcknowledegeDialog = ({
  isOpen,
  onClose,
  alarms,
  unknown,
}: AcknowledgeDialogProps) => {
  const { handleMultiAlarmAcknowledge } = useAlarms()
  const toast = useCustomToast()

  const onSubmit = () => {
    handleMultiAlarmAcknowledge(unknown ? 'ack_unknown' : 'acknowledge', {
      onSuccess: (response) => {
        const updatedAlarms = response.filter((res) => {
          return res.statusCode === 200
        })
        const alarms = updatedAlarms.length === 1 ? 'Alarm' : 'Alarms'
        const description = 'Acknowledged '
          .concat(updatedAlarms.length)
          .concat(` ${alarms}`)

        toast({
          title: `${alarms} updated`,
          description,
          status: 'success',
        })
      },
      onError: () => {
        toast({
          title: 'Update Failed',
          description: 'Failed to update alarm state',
          status: 'error',
        })
      },
      onSettled: () => {
        onClose()
      },
    })
  }

  return (
    <>
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        scrollBehavior='inside'
        size='md'
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader color='white'>
            {`Acknowledge ${alarms.length > 1 ? 'Alarms' : 'Alarm'}`}
          </ModalHeader>
          <Divider />
          <ModalCloseButton color='white' />
          <ModalBody>
            <Stack spacing={2}>
              {alarms.map((alarm) => {
                return (
                  <Flex key={alarm.alarmID} color='white'>
                    <Box p='1'>{alarm.label}</Box>
                  </Flex>
                )
              })}
            </Stack>
          </ModalBody>
          <Divider />
          <ModalFooter>
            <Button variant='outline' mr={3} onClick={onClose}>
              Cancel
            </Button>
            <Button type='submit' colorScheme='blue' onClick={onSubmit}>
              <AlarmAcknowledgedIcon />
              Confirm Acknowledge
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default AcknowledegeDialog
