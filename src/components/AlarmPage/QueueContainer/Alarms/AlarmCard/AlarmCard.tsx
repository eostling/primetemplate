import React, { ReactElement, useState } from 'react'
import { Box, Button, Flex, Stack, Text, Tooltip } from '@chakra-ui/react'
import genAlarmMetadata from '../../../../../utils/genAlarmMetadata/genAlarmMetadata'

import {
  ChevronDownIcon,
  ChevronUpIcon,
} from '../../../../../utils/generateIcons'
import { Alarm, SensorType } from '../../../../../types/index'
import AlarmStateIcon from './AlarmStateIcon/AlarmStateIcon'
import AlarmCardStateButton from './AlarmCardStateButton/AlarmCardStateButton'
import IconButtonWithTooltip from '../../../../IconButtonWithTooltip/IconButtonWithTooltip'
import { useAuth } from '../../../../../hooks/useAuth/useAuth'
import { useAlarms } from '../../../../../hooks/useAlarms/useAlarms'
import AlarmCardDetails from './AlarmCardDetails/AlarmCardDetails'

interface AlarmCardProps {
  alarm: Alarm
  onSecure: (alarm: Alarm) => void
  sensorType: SensorType
}

const AlarmCard = ({
  alarm,
  onSecure,
  sensorType,
}: AlarmCardProps): ReactElement => {
  const [show, setShow] = useState<boolean>(false)
  const { user } = useAuth()
  const { handleSingleAlarmState } = useAlarms()

  const { label, state, additionalAlarmDetails, nextValidActions } = alarm

  const hasAdditionalDetails =
    Object.values(additionalAlarmDetails).filter((v) => v !== null).length > 0

  const additionalDetailsTooltip = hasAdditionalDetails
    ? `Show ${show ? 'less' : 'more'}`
    : 'No additional info'

  const hasHiglight = ['ALARM', 'UNKNOWN'].includes(state)
  const highlightColor = hasHiglight ? `states.${state}` : ''
  const highlightWidth = hasHiglight ? '4px' : 0

  return (
    <Flex
      bg='gray.700'
      w='100%'
      flexDirection='column'
      borderRadius={3}
      borderLeftWidth={highlightWidth}
      borderTopColor={highlightColor}
      borderLeftColor={highlightColor}
      overflow='hidden'
    >
      <Flex
        p={2}
        flexDirection='row'
        alignItems='center'
        justify='space-between'
      >
        <Stack spacing={2} direction='row'>
          <Tooltip label={`Alarm state: ${state}`} placement='left'>
            <Flex>
              <AlarmStateIcon state={state} color={`states.${state}`} />
            </Flex>
          </Tooltip>
          <Tooltip label='Center on map' placement='top'>
            <Button
              maxWidth='430px'
              textAlign='left'
              overflow='hidden'
              textOverflow='ellipsis'
              display='block'
              whiteSpace='nowrap'
              variant='link'
              fontSize='.8em'
              colorScheme={`states.${state}`}
            >
              {`${label}`}
            </Button>
          </Tooltip>
        </Stack>
        <Box>
          <Stack direction='row' spacing={2}>
            <AlarmCardStateButton
              state='ACK_UNKNOWN'
              nextAction={nextValidActions.acknowledgeUnknown}
              onClick={() =>
                handleSingleAlarmState({
                  alarm,
                  newState: 'ack_unknown',
                })
              }
              user={user}
            />
            <AlarmCardStateButton
              state='ACKNOWLEDGED'
              nextAction={nextValidActions.acknowledge}
              onClick={() =>
                handleSingleAlarmState({
                  alarm,
                  newState: 'acknowledge',
                })
              }
              user={user}
            />
            <AlarmCardStateButton
              state='ACCESS'
              nextAction={nextValidActions.access}
              onClick={() =>
                handleSingleAlarmState({
                  alarm,
                  newState: 'access',
                })
              }
              user={user}
            />
            <AlarmCardStateButton
              state='SECURE'
              nextAction={nextValidActions.secure}
              onClick={() => {
                onSecure(alarm)
              }}
              user={user}
            />
            <IconButtonWithTooltip
              icon={show ? <ChevronUpIcon /> : <ChevronDownIcon />}
              label={additionalDetailsTooltip}
              placement='top'
              size='m'
              p={1}
              onClick={() => setShow(!show)}
              disabled={!hasAdditionalDetails}
              ariaLabel='alarm-card'
            />
          </Stack>
        </Box>
      </Flex>
      <Box bg='gray.900' p={2}>
        <Text as='small' d='block' whiteSpace='pre-line'>
          {genAlarmMetadata(alarm, sensorType)}
        </Text>

        {show && <AlarmCardDetails details={additionalAlarmDetails} />}
      </Box>
    </Flex>
  )
}

export default AlarmCard
