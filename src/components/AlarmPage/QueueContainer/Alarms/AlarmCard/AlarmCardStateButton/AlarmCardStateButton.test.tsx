import { render, screen } from '@testing-library/react'
import AlarmCardStateButton from './AlarmCardStateButton'

describe('<AlarmCardStateButton />', () => {
  const mockAction = {
    displayProperties: {
      isDisabled: false,
      toolTipMessage: 'test',
    },
    conditionsToMeetInOrderToDisplay: {
      hasPermissions: [],
    },
  }

  const mockUser = {
    username: 'brain',
    permissions: [],
    role: 'test',
  }
  const setup = () => {
    render(
      <AlarmCardStateButton
        onClick={jest.fn}
        state='ALARM'
        nextAction={mockAction}
        user={mockUser}
      />
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByRole('button')).toBeDefined()
  })
})
