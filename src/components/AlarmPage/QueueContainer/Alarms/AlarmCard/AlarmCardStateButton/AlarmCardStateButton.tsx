import React, { ReactElement } from 'react'
import { Box } from '@chakra-ui/react'
import IconButtonWithTooltip from '../../../../../IconButtonWithTooltip/IconButtonWithTooltip'
import { Action, User } from '../../../../../../types/index'
import AlarmStateIcon from '../AlarmStateIcon/AlarmStateIcon'

interface AlarmCardStateButtonProps {
  state: any | undefined
  nextAction: Action
  onClick: VoidFunction
  user: User
}

const AlarmCardStateButton = ({
  state,
  nextAction,
  onClick,
  user,
}: AlarmCardStateButtonProps): ReactElement | null => {
  const checkConditions = (user: User) => {
    return (
      !nextAction.conditionsToMeetInOrderToDisplay.hasPermissions ||
      nextAction.conditionsToMeetInOrderToDisplay.hasPermissions.every(
        (permission) => user.permissions.includes(permission)
      )
    )
  }

  return nextAction !== undefined && nextAction && checkConditions(user) ? (
    <Box>
      <IconButtonWithTooltip
        icon={<AlarmStateIcon state={state} />}
        label={nextAction.displayProperties.toolTipMessage}
        placement='left'
        color={`states.${state}`}
        size='m'
        p={1}
        disabled={nextAction.displayProperties.isDisabled}
        onClick={onClick}
        ariaLabel='alarm-card-state-button'
      />
    </Box>
  ) : null
}

export default AlarmCardStateButton
