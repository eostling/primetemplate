import { render, screen } from '@testing-library/react'
import AlarmCardDetails from './AlarmCardDetails'

describe('<EmptyQueue />', () => {
  const mockDetails = {
    batteryPercentage: 80,
    credentials: 'brain',
    eventID: '1234',
    confidence: 'high',
    personDetected: true,
  }
  const setup = () => {
    render(<AlarmCardDetails details={mockDetails} />)
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('Battery: 80%')).toBeDefined()
  })
})
