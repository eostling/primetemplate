import React, { ReactElement, useRef, useState } from 'react'

import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogOverlay,
  Button,
  Stack,
  Text,
} from '@chakra-ui/react'
import axios from 'axios'

interface AlarmCardDetailsProps {
  details: {
    batteryPercentage: number
    credentials: string
    eventID: string
    confidence: string
    personDetected: boolean | null
  }
}

const AlarmCardDetails = ({ details }: AlarmCardDetailsProps): ReactElement => {
  const [isOpen, setIsOpen] = useState(false)
  const ref = useRef<HTMLDivElement>(null)

  const onClose = () => setIsOpen(false)

  const {
    batteryPercentage,
    credentials,
    eventID,
    confidence,
    personDetected,
  } = details

  const startAlarmPlayback = async (eId) => {
    return axios.get(`/api/replay/event/${eId}`)
  }

  return (
    <>
      <AlertDialog
        isOpen={isOpen}
        onClose={onClose}
        isCentered
        leastDestructiveRef={ref}
      >
        <AlertDialogOverlay>
          <AlertDialogContent color='lightText'>
            <AlertDialogBody textAlign='center'>
              Event has been opened for viewing on the Vicads OwlEye Softwall
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button size='sm' onClick={onClose} color='lightText'>
                Dismiss
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      {batteryPercentage && (
        <Text as='small'>{`Battery: ${batteryPercentage}%`}</Text>
      )}
      {credentials && <Text as='small'>{`Credentials: ${credentials}`}</Text>}

      <Stack float='right'>
        {personDetected && (
          <Text as='small'>{`Person Detected: ${personDetected}`}</Text>
        )}
        {confidence && (
          <Text as='small'>{`Confidence Level: ${confidence}`}</Text>
        )}
        {eventID && (
          <Button
            onClick={() => {
              startAlarmPlayback(eventID).then((res) => {
                if (res.status === 200) {
                  return setIsOpen(true)
                }
                return null
              })
            }}
            size='sm'
            float='right'
            color='lightText'
          >
            Start Playback
          </Button>
        )}
      </Stack>
    </>
  )
}

export default AlarmCardDetails
