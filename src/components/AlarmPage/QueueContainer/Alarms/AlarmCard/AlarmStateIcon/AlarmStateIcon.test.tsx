import AlarmStateIcon from './AlarmStateIcon'

import { render } from '@testing-library/react'

describe('<EmptyQueue />', () => {
  test('renders ALARM state icon', () => {
    const { container } = render(<AlarmStateIcon state='ALARM' color='red' />)
    const icon = container.querySelector('.chakra-icon')
    expect(icon).toBeDefined()
  })
})
