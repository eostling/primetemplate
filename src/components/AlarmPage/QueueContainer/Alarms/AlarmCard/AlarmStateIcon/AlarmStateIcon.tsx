import React from 'react'
import {
  AlarmAccessIcon,
  AlarmAcknowledgedIcon,
  AlarmAcknowledgedUnknownIcon,
  AlarmAlarmIcon,
  AlarmSecureIcon,
  AlarmUnknownIcon,
} from '../../../../../../utils/generateIcons'

interface AlarmStateIconProps {
  state: string
  color?: string
}

const AlarmStateIcon = ({ state, color }: AlarmStateIconProps) => {
  if (state === 'ACCESS') return <AlarmAccessIcon color={color} />
  if (state === 'ACKNOWLEDGED') return <AlarmAcknowledgedIcon color={color} />
  if (state === 'ACK_UNKNOWN')
    return <AlarmAcknowledgedUnknownIcon color={color} />
  if (state === 'ALARM') return <AlarmAlarmIcon color={color} />
  if (state === 'SECURE') return <AlarmSecureIcon color={color} />

  return <AlarmUnknownIcon color={color} />
}

export default AlarmStateIcon
