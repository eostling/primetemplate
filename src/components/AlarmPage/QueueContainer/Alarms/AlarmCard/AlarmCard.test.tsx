import { screen } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import { renderWithProviders } from '../../../../../testUtils'
import AlarmCard from './AlarmCard'

describe('Alarm Card component', () => {
  const alarmSECURE = {
    label: 'Test Alarm - INTRUSION',
    alarmID: 'qwerty',
    lat: 0,
    lon: 10,
    state: 'SECURE',
    sensorName: 'test',
    sensorType: 'test type',
    alarmTime: 3600000,
    zone: 2,
    geohash: '12e12r1',
    priority: 1,
    facilityID: 'Test Bldg',
    floor: 1,
    facilityName: 'test facility',
    active: false,
    alarmType: 'TAMPER',
    acknowledgementTimeout: false,
    additionalAlarmDetails: {
      batteryPercentage: 80,
      confidence: '90',
      credentials: '12345',
      eventID: '123131',
      personDetected: false,
    },
    nextValidActions: {
      access: {
        displayProperties: {
          isDisabled: false,
          toolTipMessage: 'Put into Access',
        },
        conditionsToMeetInOrderToDisplay: {
          hasPermissions: [],
        },
        checkConditions() {
          return true // no conditions
        },
      },
      secure: {
        displayProperties: {
          isDisabled: false,
          toolTipMessage: 'Secure Alarm',
        },
        conditionsToMeetInOrderToDisplay: {
          hasPermissions: [],
        },
      },
      acknowledge: {
        displayProperties: {
          isDisabled: false,
          toolTipMessage: 'Acknowledge Alarm',
        },
        conditionsToMeetInOrderToDisplay: {
          hasPermissions: [],
        },
      },
      acknowledgeUnknown: {
        displayProperties: {
          isDisabled: false,
          toolTipMessage: 'Acknowledge Unknown',
        },
        conditionsToMeetInOrderToDisplay: {
          hasPermissions: [],
        },
      },
    },
  }

  const mockSensorType = {
    id: 'test sensor',
    make: 'make',
    model: 'model',
    type: 'PIR',
    description: null,
    events: [],
  }

  const onSecure = jest.fn()

  const setup = () => {
    renderWithProviders(
      <BrowserRouter>
        <AlarmCard
          alarm={alarmSECURE}
          onSecure={onSecure}
          sensorType={mockSensorType}
        />
      </BrowserRouter>
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('Test Alarm - INTRUSION')).toBeDefined()
  })
})
