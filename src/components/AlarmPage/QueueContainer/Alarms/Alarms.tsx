import React from 'react'
import { ReactElement } from 'react'
import { Virtuoso } from 'react-virtuoso'
import AlarmCard from './AlarmCard/AlarmCard'

import { Flex, Stack } from '@chakra-ui/react'
import { Alarm, SensorType } from '../../../../types'
import MultiAlarmStateButtons from '../MultiAlarmStateButtons/MultiAlarmStateButtons'

import { useSensorTypes } from '../../../../hooks/useSensorTypes/useSensorTypes'

interface AlarmsProps {
  alarms: Alarm[]
  handleSecureSingle: (alarm: Alarm) => void
  handleSecureMulti: (newState: string, action: string) => void
  filterAlarmsByNextAction: (action: string) => Alarm[]
}

interface ListItemProps {
  index: number
}

const Alarms = ({
  alarms,
  handleSecureSingle,
  handleSecureMulti,
  filterAlarmsByNextAction,
}: AlarmsProps): ReactElement => {
  const sensorTypes = useSensorTypes()

  const ListItem = ({ index }: ListItemProps) => {
    const alarm = alarms[index]

    const sensorType = sensorTypes.filter(
      (sensorType: SensorType) => sensorType.id === alarm.sensorType
    )[0]

    return (
      <Flex mb={2} key={alarm.alarmID}>
        <AlarmCard
          alarm={alarm}
          onSecure={handleSecureSingle}
          sensorType={sensorType}
        />
      </Flex>
    )
  }

  const itemContent = (index: number) => {
    return <ListItem index={index} />
  }

  return (
    <>
      {alarms && (
        <>
          <Flex justify='flex-end' placement='top'>
            <MultiAlarmStateButtons
              handleSecureMulti={handleSecureMulti}
              filterAlarmsByNextAction={filterAlarmsByNextAction}
            />
          </Flex>
          {alarms.length > 0 && (
            <Stack spacing={2} flexGrow={1} overflow='auto'>
              <Virtuoso data={alarms} itemContent={itemContent} />
            </Stack>
          )}
        </>
      )}
    </>
  )
}

export default Alarms
