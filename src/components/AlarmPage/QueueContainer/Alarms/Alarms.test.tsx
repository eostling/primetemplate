import { screen } from '@testing-library/react'
import { renderWithProviders } from '../../../../testUtils'
import Alarms from './Alarms'

describe('<Alarms />', () => {
  const mockFilterAlarmsByNextAction = () => []

  const setup = () => {
    renderWithProviders(
      <Alarms
        handleSecureMulti={jest.fn}
        handleSecureSingle={jest.fn}
        filterAlarmsByNextAction={mockFilterAlarmsByNextAction}
        alarms={[]}
      />
    )
  }

  test('should render without error', () => {
    setup()
    const items = screen.getAllByRole('button')
    expect(items).toHaveLength(3)
  })
})
