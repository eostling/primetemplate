import { render, screen } from '@testing-library/react'
import { Tabs, TabList } from '@chakra-ui/react'
import Tab from './Tab'

describe('<Tab/>', () => {
  const setup = () => {
    render(
      <Tabs>
        <TabList>
          <Tab label='test' badgeContent={5} />
        </TabList>
      </Tabs>
    )
  }

  test('renders tab label', () => {
    setup()
    expect(screen.getByText('test')).toBeDefined()
  })

  test('renders badge content', () => {
    setup()
    expect(screen.getByText(5)).toBeDefined()
  })
})
