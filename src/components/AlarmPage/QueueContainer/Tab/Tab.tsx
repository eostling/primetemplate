import React from 'react'

import { Tab as ChakraTab, Text, HStack, Badge } from '@chakra-ui/react'

interface TabProps {
  label: string
  badgeColorScheme?: string
  badgeContent: string | number
}

const Tab = ({ label, badgeColorScheme, badgeContent }: TabProps) => {
  return (
    <ChakraTab _focus={{ outlineStyle: 'none' }}>
      <HStack spacing={2}>
        <Text>{label}</Text>
        <Badge colorScheme={badgeColorScheme}>{badgeContent}</Badge>
      </HStack>
    </ChakraTab>
  )
}

export default Tab
