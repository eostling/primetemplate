import React, { ReactElement } from 'react'
import FacilityQueue from './FacilityQueue/FacilityQueue'
import BaseQueue from './BaseQueue/BaseQueue'
import { TabPanels, Tabs, TabList, TabPanel, Flex, Box } from '@chakra-ui/react'
import Tab from './Tab/Tab'
import { useFacilities } from '../../../hooks/useFacilities/useFacilities'
import { useAlarms } from '../../../hooks/useAlarms/useAlarms'

const QueueContainer = (): ReactElement => {
  const { activeFacilities, allFacilities } = useFacilities()
  const { activeAlarms, accessAlarms, allAlarms } = useAlarms()

  const alarmsExist = allAlarms.length > 0

  return (
    <Flex
      direction='column'
      width='40em'
      bgGradient='linear(to-b, gray.800, gray.900)'
      p={2}
    >
      <Box height='100%' width='100%' overflow='hidden'>
        <Tabs
          height='100%'
          display='flex'
          flexDirection='column'
          position='relative'
          className='alarm_spec'
        >
          <TabList>
            <Tab
              label='Alarms'
              badgeContent={activeAlarms.length}
              badgeColorScheme='red'
            />
            <Tab
              label='Access'
              badgeContent={accessAlarms.length}
              badgeColorScheme='yellow'
            />
            <Tab label='Facilities' badgeContent={allFacilities.length} />
          </TabList>
          <TabPanels overflow='hidden' height='100%'>
            <TabPanel height='100%'>
              <BaseQueue
                queueType='active'
                alarms={activeAlarms}
                alarmsExist={alarmsExist}
              />
            </TabPanel>
            <TabPanel height='100%'>
              <BaseQueue
                queueType='access'
                alarms={accessAlarms}
                alarmsExist={alarmsExist}
              />
            </TabPanel>
            <TabPanel height='100%'>
              <FacilityQueue
                activeFacilities={activeFacilities}
                allFacilities={allFacilities}
              />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </Flex>
  )
}

export default QueueContainer
