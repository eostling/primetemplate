import { ReactElement } from 'react'
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
} from '@chakra-ui/react'
import titleCase from '../../../../utils/titleCase/titleCase'
interface EmptyQueueProps {
  queueType: string
  itemSubType?: string
  complexFilter?: boolean
}

const EmptyQueueMessage = ({
  queueType,
  itemSubType,
  complexFilter,
}: EmptyQueueProps): ReactElement => {
  const alertStatus = queueType === 'alarm' ? 'success' : 'info'

  return (
    <Alert
      status={alertStatus}
      variant='subtle'
      position='static'
      flexDirection='column'
      justifyContent='center'
      textAlign='center'
      height='200px'
      minWidth='100%'
      borderRadius='lg'
    >
      <AlertIcon />
      <AlertTitle mt={4} mb={1} fontSize='lg'>
        {queueType === 'alarm' && titleCase(`${queueType} queue empty!`)}
        {queueType === 'facilities' && titleCase(`no matching ${queueType}`)}
      </AlertTitle>
      <AlertDescription maxWidth='sm'>
        {complexFilter
          ? `There are no ${queueType} matching your filters`
          : `There are no ${itemSubType} that require your attention.`}
      </AlertDescription>
    </Alert>
  )
}

export default EmptyQueueMessage
