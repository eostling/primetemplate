import { render, screen } from '@testing-library/react'
import EmptyQueue from './EmptyQueueMessage'

describe('<EmptyQueue />', () => {
  const setup = () => {
    render(<EmptyQueue queueType='alarm' itemSubType='Active Alarms' />)
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText(/Alarm Queue Empty!/i)).toBeDefined()
  })
})
