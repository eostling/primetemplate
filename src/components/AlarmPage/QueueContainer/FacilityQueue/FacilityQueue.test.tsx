import { render, screen } from '@testing-library/react'
import FacilityQueue from './FacilityQueue'

describe('<FacilityQueue />', () => {
  const setup = () => {
    render(<FacilityQueue activeFacilities={[]} allFacilities={[]} />)
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('No Matching Facilities')).toBeDefined()
  })
})
