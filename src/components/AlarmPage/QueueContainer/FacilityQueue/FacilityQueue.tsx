import React, { useState } from 'react'
import { Stack, Flex, FormLabel } from '@chakra-ui/react'
import { Virtuoso } from 'react-virtuoso'
import EmptyQueueBox from '../EmptyQueueMessage/EmptyQueueMessage'
import FacilityCard from './FacilityCard/FacilityCard'
import { Facility } from '../../../../types'
import FacilityFilter from './FacilityFilter/FacilityFilter'
import pluralize from '../../../../utils/pluralize/pluralize'
import titleCase from '../../../../utils/titleCase/titleCase'

interface FacilityQueueProps {
  activeFacilities: Facility[]
  allFacilities: Facility[]
}

interface ListItemProps {
  index: number
}

const FacilityQueue = ({
  activeFacilities,
  allFacilities,
}: FacilityQueueProps) => {
  const [currentTextFilter, setCurrentTextFilter] = useState('')
  const [currentStateFilter, setCurrentStateFilter] = useState('ACTIVE')

  const searchFacilities = (arr) => {
    const searched = arr.filter((facility) => {
      return (
        facility.facilityID.includes(currentTextFilter) ||
        facility.displayName
          .toLowerCase()
          .includes(currentTextFilter.toLowerCase())
      )
    })
    return searched
  }

  const getFilteredFacilities = () => {
    const facilitiesToSearch =
      currentStateFilter === 'ALL' || currentStateFilter === 'UNKNOWN'
        ? allFacilities
        : activeFacilities
    const searchedFacilities = currentTextFilter
      ? searchFacilities(facilitiesToSearch)
      : facilitiesToSearch

    if (currentStateFilter === 'ACTIVE') {
      return searchedFacilities.filter((facility) => facility)
    }
    if (currentStateFilter === 'ALL') {
      return searchedFacilities.filter((facility) => facility)
    }
    if (currentStateFilter === 'UNKNOWN') {
      return searchedFacilities.filter(
        (facility) => facility.state === currentStateFilter
      )
    }
    return searchedFacilities.filter(
      (facility) => facility.state === currentStateFilter
    )
  }

  const facilityCount = getFilteredFacilities().length

  const handleTextFilterChange = (val) => {
    setCurrentTextFilter(val)
  }

  const handleStateFilterChange = (val) => {
    setCurrentStateFilter(val)
  }

  const itemLabel = pluralize('facility')(facilityCount)
  const labelCount = facilityCount === 0 ? 'No' : facilityCount
  const labelText = titleCase(`${labelCount}  ${itemLabel}`)

  const ListItem = React.memo(({ index }: ListItemProps) => {
    const facility = getFilteredFacilities()[index]
    return (
      <Flex mb={2} key={facility.facilityID}>
        <FacilityCard facility={facility} />
      </Flex>
    )
  })

  const itemContent = (index: number) => {
    return <ListItem index={index} />
  }

  return (
    <Stack spacing={2} height='inherit'>
      <Flex justifyContent='space-between' alignItems='center' py={1}>
        <FormLabel data-testid='queue'>{labelText}</FormLabel>
        <FacilityFilter
          onTextFilterChange={handleTextFilterChange}
          onStateFilterChange={handleStateFilterChange}
          stateFilter={currentStateFilter}
          textFilter={currentTextFilter}
        />
      </Flex>
      {facilityCount > 0 ? (
        <Stack spacing={2} flexGrow='1' overflow='auto'>
          <Virtuoso data={getFilteredFacilities()} itemContent={itemContent} />
        </Stack>
      ) : (
        <EmptyQueueBox queueType='facilities' complexFilter />
      )}
    </Stack>
  )
}

export default FacilityQueue
