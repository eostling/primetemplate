import { render, screen } from '@testing-library/react'
import FacilityFilter from './FacilityFilter'

describe('<FacilityFilter />', () => {
  const setup = () => {
    render(
      <FacilityFilter
        stateFilter='ACTIVE'
        textFilter=''
        onStateFilterChange={jest.fn}
        onTextFilterChange={jest.fn}
      />
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('ACTIVE')).toBeDefined()
  })
})
