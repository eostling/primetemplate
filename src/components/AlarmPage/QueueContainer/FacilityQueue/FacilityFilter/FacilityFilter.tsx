import React, { ChangeEvent, ReactElement } from 'react'

import {
  Flex,
  HStack,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Select,
  Tooltip,
} from '@chakra-ui/react'
import {
  CloseCircleIcon,
  FilterIcon,
  SearchIcon,
} from '../../../../../utils/generateIcons'

interface FacilityFilterProps {
  stateFilter: string
  onStateFilterChange: (val) => void
  textFilter: string
  onTextFilterChange: (val) => void
}

const FacilityFilter = ({
  stateFilter,
  onStateFilterChange,
  textFilter,
  onTextFilterChange,
}: FacilityFilterProps): ReactElement => {
  const handleStateFilterChange = (evt: ChangeEvent<HTMLSelectElement>) => {
    onStateFilterChange(evt.target.value)
  }

  const handleTextFilterChange = (evt: ChangeEvent<HTMLInputElement>) => {
    onTextFilterChange(evt.target.value)
  }

  const handleClearTextFilter = () => {
    onTextFilterChange('')
  }

  return (
    <Flex>
      <HStack width='100%' spacing={2}>
        <Tooltip label='Filter by facility name or bldg. no.' placement='top'>
          <InputGroup>
            <InputLeftElement pointerEvents='none'>
              <SearchIcon color='gray.300' />
            </InputLeftElement>
            <Input
              type='text'
              placeholder='Search Facilities'
              value={textFilter}
              onChange={handleTextFilterChange}
            />
            <InputRightElement>
              {textFilter !== '' && (
                <IconButton
                  aria-label='Clear search text'
                  variant='ghost'
                  size='xs'
                  icon={<CloseCircleIcon />}
                  onClick={handleClearTextFilter}
                />
              )}
            </InputRightElement>
          </InputGroup>
        </Tooltip>
        <Tooltip label='Filter by facility state' placement='top'>
          <Select
            bg='bgColor'
            color='lightText'
            icon={<FilterIcon />}
            onChange={handleStateFilterChange}
            value={stateFilter}
          >
            <option>ACTIVE</option>
            <option>ALL</option>
            <option>ACCESS</option>
            <option>ACKNOWLEDGED</option>
            <option>ALARM</option>
            <option>SECURE</option>
            <option>UNKNOWN</option>
          </Select>
        </Tooltip>
      </HStack>
    </Flex>
  )
}

export default FacilityFilter
