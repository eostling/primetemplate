import { screen, render } from '@testing-library/react'
import FacilityCard from './FacilityCard'

describe('<FacilityCard />', () => {
  const facility = {
    facilityID: '1600',
    displayName: 'Bathroom',
    lat: 10,
    lon: 15,
    numFloors: 2,
    zoneID: 6,
    state: 'UNKNOWN',
    cadFilenames: { 1: 'one', 2: 'two' },
    geohash: 'aaa',
    streetAddress: 'Facility Street',
    canAccess: false,
    canSecure: false,
    count: 1,
    number: 1,
    streetaddress: '',
  }

  const setup = () => render(<FacilityCard facility={facility} />)

  test('renders without error', () => {
    setup()
    expect(screen.getByText('Bldg. 1600 - Bathroom')).toBeDefined()
  })
})
