import React from 'react'
import { Flex, Button, Stack, Tooltip } from '@chakra-ui/react'
import { Facility } from '../../../../../types'
import { FacilitiesIcon } from '../../../../../utils/generateIcons'

interface FacilityCardProps {
  facility: Facility
}

const FacilityCard = ({ facility }: FacilityCardProps) => {
  const { displayName, facilityID, state } = facility

  return (
    <Flex
      bg='gray.700'
      w='100%'
      flexDirection='column'
      borderRadius={2}
      overflow='hidden'
    >
      <Flex p={2} alignItems='center' justify='space-between'>
        <Stack spacing={2} direction='row'>
          <Tooltip label={`Facility state: ${state}`} placement='left'>
            <FacilitiesIcon color={`states.${state}`} />
          </Tooltip>
          <Tooltip label='Center on map' placement='top'>
            <Button
              maxWidth='430px'
              textAlign='left'
              overflow='hidden'
              textOverflow='ellipsis'
              display='block'
              whiteSpace='nowrap'
              variant='link'
              fontSize='.8em'
            >
              {`Bldg. ${facilityID} - ${displayName}`}
            </Button>
          </Tooltip>
        </Stack>
      </Flex>
    </Flex>
  )
}

export default FacilityCard
