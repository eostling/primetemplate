import { render, screen } from '@testing-library/react'
import MultiAlarmStateButtons from './MultiAlarmStateButtons'

describe('<MultiAlarmStateButtons />', () => {
  const mockFilterAlarmsByNextAction = () => []
  const mockHandleSecureMulti = jest.fn()

  const setup = () => {
    render(
      <MultiAlarmStateButtons
        filterAlarmsByNextAction={mockFilterAlarmsByNextAction}
        handleSecureMulti={mockHandleSecureMulti}
      />
    )
  }

  test('Should render without error', async () => {
    setup()
    const items = await screen.findAllByRole('button')
    expect(items).toHaveLength(3)
  })
})
