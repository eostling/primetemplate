import { ReactElement } from 'react'
import { Flex } from '@chakra-ui/react'
import {
  AlarmAcknowledgedUnknownIcon,
  AlarmAcknowledgedIcon,
  AlarmSecureIcon,
} from '../../../../utils/generateIcons'
import IconButtonWithTooltip from '../../../IconButtonWithTooltip/IconButtonWithTooltip'
import { Alarm } from '../../../../types'

interface MultiAlarmStateButtonsProps {
  handleSecureMulti: (newState: string, action: string) => void
  filterAlarmsByNextAction: (action: string) => Alarm[]
}

const MultiAlarmStateButtons = ({
  handleSecureMulti,
  filterAlarmsByNextAction,
}: MultiAlarmStateButtonsProps): ReactElement => {
  return (
    <>
      <Flex justify='flex-end' placement='top'>
        <IconButtonWithTooltip
          label='Acknowledge All Unknown'
          placement='top'
          icon={<AlarmAcknowledgedUnknownIcon />}
          onClick={() => handleSecureMulti('ACK_UNKNOWN', 'acknowledgeUnknown')}
          disabled={filterAlarmsByNextAction('acknowledgeUnknown').length === 0}
          mr={1}
          color='states.ACK_UNKNOWN'
          colorScheme='states.ACK_UNKNOWN'
          variant='outline'
          ariaLabel='acknowledge all unknown'
        />
        <IconButtonWithTooltip
          label='Acknowledge All'
          placement='top'
          icon={<AlarmAcknowledgedIcon />}
          onClick={() => handleSecureMulti('ACKNOWLEDGED', 'acknowledge')}
          disabled={filterAlarmsByNextAction('acknowledge').length === 0}
          mr={1}
          color='states.ACKNOWLEDGED'
          colorScheme='states.ACKNOWLEDGED'
          variant='outline'
          ariaLabel='acknowledge all alarming'
        />
        <IconButtonWithTooltip
          label='Secure All'
          placement='top'
          icon={<AlarmSecureIcon />}
          onClick={() => handleSecureMulti('SECURE', 'secure')}
          disabled={filterAlarmsByNextAction('secure').length === 0}
          mr={1}
          color='states.SECURE'
          colorScheme='states.SECURE'
          variant='outline'
          ariaLabel='secure all'
        />
      </Flex>
    </>
  )
}

export default MultiAlarmStateButtons
