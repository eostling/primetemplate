import { screen } from '@testing-library/react'
import { renderWithProviders } from '../../../../testUtils'
import BaseQueue from './BaseQueue'

describe('<BaseQueue />', () => {
  const queueType = 'Active'
  const alarms: any[] = []
  const setup = () => {
    renderWithProviders(
      <BaseQueue queueType={queueType} alarms={alarms} alarmsExist={false} />
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('Loading...')).toBeDefined()
  })
})
