import React, { ReactElement, useCallback, useState, useContext } from 'react'

import Alarms from '../Alarms/Alarms'
import EmptyQueueMessage from '../EmptyQueueMessage/EmptyQueueMessage'

import {
  Flex,
  FormLabel,
  Spinner,
  Stack,
  Text,
  useDisclosure,
} from '@chakra-ui/react'
import { Alarm } from '../../../../types'
import SecureDialog from '../SecureDialog/SecureDialog'
import AcknowledegeDialog from '../AcknowledgeDialog/AcknowledgeDialog'
import { SocketContext } from '../../../../context/socket'
import pluralize from '../../../../utils/pluralize/pluralize'
import titleCase from '../../../../utils/titleCase/titleCase'

interface BaseQueueProps {
  queueType: string
  alarms: Alarm[]
  alarmsExist: boolean
}

const BaseQueue = ({
  queueType,
  alarms,
  alarmsExist,
}: BaseQueueProps): ReactElement => {
  const {
    isOpen: isMultiOpen,
    onOpen: onMultiOpen,
    onClose: onMultiClose,
  } = useDisclosure()

  const {
    isOpen: isSingleOpen,
    onOpen: onSingleOpen,
    onClose: onSingleClose,
  } = useDisclosure()

  const {
    isOpen: unknownOpen,
    onOpen: onAckUnknownOpen,
    onClose: onAckUnknownClose,
  } = useDisclosure()

  const {
    isOpen: isAcknowledgeOpen,
    onOpen: onAcknowledgeOpen,
    onClose: onAcknowledgeClose,
  } = useDisclosure()

  const socket = useContext(SocketContext)

  const [unknownAlarmList, setUnknownAlarmList] = useState<Alarm[]>([])
  const [acknowledgeList, setAcknowledgeList] = useState<Alarm[]>([])
  const [secureAlarmList, setSecureAlarmList] = useState<Alarm[]>([])
  const [singleAlarm, setSingleAlarm] = useState<Alarm[]>([])
  const [connectedToServer, setConnectedToServer] = useState<boolean>(true)

  const filterAlarmsByNextAction = (action) => {
    return alarms.filter(
      (alarm) =>
        alarm.nextValidActions[action] &&
        !alarm.nextValidActions[action].displayProperties.isDisabled
    )
  }

  const handleSecureMulti = (newState, action) => {
    const filteredAlarms = filterAlarmsByNextAction(action)
    if (newState === 'SECURE') {
      setSecureAlarmList(filteredAlarms)
      onMultiOpen()
    } else if (newState === 'ACKNOWLEDGED') {
      setAcknowledgeList(filteredAlarms)
      onAcknowledgeOpen()
    } else if (newState === 'ACK_UNKNOWN') {
      setUnknownAlarmList(filteredAlarms)
      onAckUnknownOpen()
    }
  }

  const handleSecureSingle = useCallback(
    (alarm) => {
      setSingleAlarm([alarm])
      onSingleOpen()
    },
    [onSingleOpen]
  )

  socket.on('disconnect', () => {
    setConnectedToServer(false)
  })

  const itemLabel = pluralize('alarm')(alarms.length)
  const labelCount = alarms.length === 0 ? 'No' : alarms.length
  const labelText = titleCase(`${labelCount} ${queueType} ${itemLabel}`)

  return (
    <>
      {connectedToServer && alarmsExist ? (
        <Flex height='inherit' direction='column'>
          <Stack spacing={2} height='inherit'>
            <Flex alignItems='center' justifyContent='space-between'>
              <FormLabel data-testid='queue'>{labelText}</FormLabel>
            </Flex>
            {alarms.length > 0 ? (
              <Alarms
                alarms={alarms}
                handleSecureSingle={handleSecureSingle}
                handleSecureMulti={handleSecureMulti}
                filterAlarmsByNextAction={filterAlarmsByNextAction}
              />
            ) : (
              <EmptyQueueMessage
                queueType='alarm'
                itemSubType='active alarms'
              />
            )}
            <SecureDialog
              isOpen={isSingleOpen}
              onClose={onSingleClose}
              alarms={singleAlarm}
            />
            <SecureDialog
              isOpen={isMultiOpen}
              onClose={onMultiClose}
              alarms={secureAlarmList}
            />
            <AcknowledegeDialog
              isOpen={isAcknowledgeOpen}
              onClose={onAcknowledgeClose}
              alarms={acknowledgeList}
              unknown={false}
            />
            <AcknowledegeDialog
              isOpen={unknownOpen}
              onClose={onAckUnknownClose}
              alarms={unknownAlarmList}
              unknown
            />
          </Stack>
        </Flex>
      ) : (
        <Flex alignItems='center' justifyContent='center'>
          <Spinner mr={2} />
          <Text>Loading</Text>
        </Flex>
      )}
    </>
  )
}

export default BaseQueue
