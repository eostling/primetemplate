// TODO: A better way of doing this

import { useContext } from 'react'
import { useQueryClient } from 'react-query'
import { SocketContext } from '../../context/socket'
import useAlarmAudio from '../../hooks/useAlarmAudio/useAlarmAudio'
import IdleTimer from '../IdleTimer/IdleTimer'

const GlobalWatcher = () => {
  const socket = useContext(SocketContext)
  const queryClient = useQueryClient()

  socket.on('alarm', () => {
    queryClient.invalidateQueries('allAlarms')
    queryClient.invalidateQueries('activeAlarms')
    queryClient.invalidateQueries('accessAlarms')
  })

  socket.on('facility', () => {
    queryClient.invalidateQueries('activeFacilities')
    queryClient.invalidateQueries('allFacilities')
  })

  useAlarmAudio()

  return <IdleTimer />
}

export default GlobalWatcher
