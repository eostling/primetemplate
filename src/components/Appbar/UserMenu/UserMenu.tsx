import React, { ReactElement } from 'react'
import {
  Button,
  Menu,
  MenuButton,
  MenuDivider,
  MenuList,
  MenuItem,
  Tooltip,
  useDisclosure,
  Avatar,
} from '@chakra-ui/react'
import UserProfile from './UserProfile/UserProfile'

import {
  ChevronDownIcon,
  GearIcon,
  LogoutIcon,
} from '../../../utils/generateIcons'
import { useAuth } from '../../../hooks/useAuth/useAuth'

const UserMenu = (): ReactElement => {
  const { user, handleLogout } = useAuth()
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <Menu>
      <Tooltip label='User Menu' placement='bottom'>
        <MenuButton
          as={Button}
          leftIcon={<Avatar size='sm' name={user.username} />}
          rightIcon={<ChevronDownIcon />}
          variant='ghost'
        >
          {user.username}
        </MenuButton>
      </Tooltip>
      <MenuList placement='bottom-end' zIndex='popover'>
        <MenuItem onClick={onOpen}>
          <GearIcon mr={2} />
          User Settings
        </MenuItem>
        <MenuDivider />
        <MenuItem onClick={() => handleLogout()}>
          <LogoutIcon mr={2} />
          Sign Out
        </MenuItem>
      </MenuList>
      <UserProfile isOpen={isOpen} onClose={onClose} user={user} />
    </Menu>
  )
}

export default UserMenu
