import { screen } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import { renderWithProviders } from '../../../testUtils'
import UserMenu from './UserMenu'

jest.mock('../../../hooks/useAuth/useAuth', () => ({
  useAuth: () => ({ user: { username: 'brain', permissions: [] } }),
}))

describe('<UserMenu />', () => {
  const setup = () => {
    renderWithProviders(
      <BrowserRouter>
        <UserMenu />
      </BrowserRouter>
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('brain')).toBeDefined()
  })
})
