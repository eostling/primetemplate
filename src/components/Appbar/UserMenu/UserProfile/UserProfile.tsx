import React, { ReactElement, useRef } from 'react'

import {
  Button,
  Divider,
  Flex,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Stack,
  Tag,
  Wrap,
  WrapItem,
  Avatar,
} from '@chakra-ui/react'

import { User } from '../../../../types/index'

interface UserProfileProps {
  isOpen: boolean
  onClose: VoidFunction
  user: User
}

const UserProfile = ({
  isOpen,
  onClose,
  user,
}: UserProfileProps): ReactElement => {
  const name = user && user.username
  const initialFocus = useRef()

  const RED_PERM: string[] = ['DEVELOPMENT']
  const YLW_PERM: string[] = ['ACCESS']
  const BLUE_PERM: string[] = [
    'CONTACT_MANAGEMENT',
    'AUDIT',
    'FACILITY_MANAGEMENT',
    'SENSOR_MANAGEMENT',
    'SETTINGS_MANAGEMENT',
    'USER_MANAGEMENT',
  ]

  const getColor = (permission: string): string => {
    if (RED_PERM.includes(permission)) {
      return 'red'
    }
    if (YLW_PERM.includes(permission)) {
      return 'yellow'
    }
    if (BLUE_PERM.includes(permission)) {
      return 'blue'
    }
    return 'gray'
  }

  return (
    <Modal
      initialFocusRef={initialFocus.current}
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>
          <Avatar name={name} size='sm' mr={2} />
          {name}
        </ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Stack>
            <Flex color='white' fontWeight='bold'>
              User Permissions
            </Flex>
          </Stack>
          <Divider m={2} />
          <Stack>
            <Wrap>
              {user &&
                user.permissions.map((permission: string) => {
                  return (
                    <WrapItem key={`item-${permission}`}>
                      <Tag
                        colorScheme={getColor(permission)}
                        key={permission}
                        data-testid='permissions'
                      >
                        {permission}
                      </Tag>
                    </WrapItem>
                  )
                })}
            </Wrap>
          </Stack>
        </ModalBody>
        <ModalFooter>
          <Button mr={3} onClick={onClose}>
            Done
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}

export default UserProfile
