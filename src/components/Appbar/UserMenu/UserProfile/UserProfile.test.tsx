import { render, screen } from '@testing-library/react'
import UserProfile from './UserProfile'

describe('<UserProfile />', () => {
  const setup = () => {
    render(
      <UserProfile
        isOpen={true}
        onClose={jest.fn}
        user={{ username: 'brain', permissions: [], role: 'admin' }}
      />
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText(/User Permissions/i)).toBeDefined()
  })
})
