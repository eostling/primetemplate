import { render, screen } from '@testing-library/react'
import Version from './Version'

describe('<Version />', () => {
  const originalEnv = process.env

  beforeEach(() => {
    jest.resetModules()
    process.env = {
      ...originalEnv,
      REACT_APP_VERSION_NUMBER: '1.0.7',
    }
  })

  afterEach(() => {
    process.env = originalEnv
  })

  const setup = () => {
    render(<Version />)
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText('v1.0.7')).toBeDefined()
  })
})
