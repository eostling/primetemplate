import React from 'react'
import { Box, Text } from '@chakra-ui/react'

const Version = (): React.ReactElement => {
  const version = process.env.REACT_APP_VERSION_NUMBER
  return (
    <Box title={`Version ${version}`}>
      <Text color='gray.400' fontSize='xs' p='.2em'>{`v${version}`}</Text>
    </Box>
  )
}

export default Version
