import React, { ReactElement } from 'react'
import { Flex, Heading } from '@chakra-ui/react'

import { AirForceIcon } from '../../utils/generateIcons'
import Clock from './Clock/Clock'
import UserMenu from './UserMenu/UserMenu'
import Version from './Version/Version'

const Appbar = (): ReactElement => {
  return (
    <Flex
      as='nav'
      bgGradient='linear(135deg, gray.600, gray.700)'
      alignItems='center'
      justifyContent='space-between'
    >
      <Flex alignItems='center'>
        <AirForceIcon ml={3} mr={7} h='2.5em' w='2.5em' />
        <Flex alignItems='flex-end'>
          <Heading fontSize='2em' letterSpacing='0.15em'>
            {process.env.REACT_APP_NAME}
          </Heading>
          <Version />
        </Flex>
      </Flex>
      <Flex alignItems='center' p={2}>
        <Clock />
        <UserMenu />
      </Flex>
    </Flex>
  )
}

export default Appbar
