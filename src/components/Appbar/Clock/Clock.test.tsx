import { render, screen } from '@testing-library/react'
import Clock from './Clock'

describe('<Clock />', () => {
  const setup = () => {
    render(<Clock />)
  }

  test('should render without error', () => {
    setup()
    expect(
      screen.getByText(/^\d{1,2}\/\d{1,2}\/\d{4} \d{1,2}:\d{1,2}:\d{1,2}/i)
    ).toBeDefined()
  })
})
