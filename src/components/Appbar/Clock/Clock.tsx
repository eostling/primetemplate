import React, { useLayoutEffect, useState } from 'react'
import { DateTime } from 'luxon'

import { Box, Text } from '@chakra-ui/react'

const getNow = () => {
  return DateTime.now().toFormat('M/d/yyyy HH:mm:ss')
}

const Clock = () => {
  const [now, setNow] = useState(getNow())

  useLayoutEffect(() => {
    const initialTimeout = 60 - parseInt(DateTime.now().toFormat('ss'))

    const timeout = setTimeout(() => {
      setNow(getNow())
      setInterval(() => setNow(getNow()), 10)
    }, initialTimeout)

    return () => {
      clearTimeout(timeout)
    }
  }, [])

  return (
    <Box>
      <Text fontSize='md'>{now}</Text>
    </Box>
  )
}

export default Clock
