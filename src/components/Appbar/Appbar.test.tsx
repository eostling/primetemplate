import { screen } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import { renderWithProviders } from '../../testUtils'
import Appbar from './Appbar'

jest.mock('../../hooks/useAuth/useAuth', () => ({
  useAuth: () => ({ user: { username: 'brain', permissions: [] } }),
}))

describe('<Appbar />', () => {
  const setup = () => {
    renderWithProviders(
      <BrowserRouter>
        <Appbar />
      </BrowserRouter>
    )
  }

  test('should render without error', () => {
    setup()
    expect(screen.getByText(/User Settings/i)).toBeDefined()
  })
})
