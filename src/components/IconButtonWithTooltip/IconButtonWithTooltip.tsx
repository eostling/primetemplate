import React, {
  CSSProperties,
  JSXElementConstructor,
  ReactElement,
} from 'react'
import { Box, IconButton, Tooltip } from '@chakra-ui/react'

interface IconButtonWithTooltipProps {
  label: string
  disabled?: boolean
  placement: any | undefined
  icon?: ReactElement<any, string | JSXElementConstructor<any>> | undefined
  onClick: VoidFunction
  ariaLabel: string
  colorScheme?: string
  color?: string
  tooltipProp?: Record<any, any>
  mr?: string | number
  p?: any | unknown
  rest?: Record<any, any>
  size?: string
  style?: CSSProperties
  variant?: string
  loading?: boolean
  visibility?: string
}

const IconButtonWithTooltip = ({
  label,
  placement,
  disabled,
  tooltipProp,
  ariaLabel,
  colorScheme,
  color,
  icon,
  mr,
  onClick,
  p,
  rest,
  size,
  style,
  variant,
  loading,
}: IconButtonWithTooltipProps): React.ReactElement => {
  if (color) {
    return (
      <Tooltip label={label} placement={placement} tooltipProp={tooltipProp}>
        <Box cursor={disabled ? 'not-allowed' : 'inherit'}>
          <IconButton
            disabled={disabled}
            pointerEvents={disabled ? 'none' : 'inherit'}
            icon={icon}
            aria-label={ariaLabel}
            colorScheme={colorScheme}
            color={color}
            mr={mr}
            onClick={onClick}
            p={p}
            rest={rest}
            size={size}
            style={style}
            variant={variant}
            isLoading={loading}
            data-testid='iconwithbutton'
          />
        </Box>
      </Tooltip>
    )
  }
  return (
    <Tooltip label={label} placement={placement} tooltipProp={tooltipProp}>
      <Box d='inline' cursor={disabled ? 'not-allowed' : 'inherit'}>
        <IconButton
          disabled={disabled}
          pointerEvents={disabled ? 'none' : 'inherit'}
          icon={icon}
          aria-label={ariaLabel}
          mr={mr}
          onClick={onClick}
          p={p}
          rest={rest}
          size={size}
          style={style}
          variant={variant}
          isLoading={loading}
        />
      </Box>
    </Tooltip>
  )
}

export default IconButtonWithTooltip
