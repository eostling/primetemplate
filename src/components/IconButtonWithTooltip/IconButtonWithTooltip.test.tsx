import React from 'react'
import { render, screen } from '@testing-library/react'
import IconButtonWithTooltip from './IconButtonWithTooltip'

describe('<IconButtonWithTooltip />', () => {
  const setup = () => {
    render(
      <IconButtonWithTooltip
        disabled={false}
        ariaLabel='label'
        icon={<></>}
        placement='top'
        aria-label='label'
        colorScheme='blue'
        label='label'
        color='blue'
        mr={2}
        onClick={jest.fn}
        p={2}
        size='size'
        style={undefined}
        variant='variant'
        loading={false}
      />
    )
  }

  test('Should render without error', () => {
    setup()
    expect(screen.getByRole('button')).toBeDefined()
  })
})
