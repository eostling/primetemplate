TOKEN_SECRET ?= novetta
.PHONY: debug dist njsscan install clean reinstall dev lint test test-coverage snapshots

# Set the Image version to the branch if environment variable is not set
IMAGE_VERSION ?= latest
GIT_COMMIT := $(shell git log -1 --format=%h)
GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)

dist:
	DOCKER_BUILDKIT=1 docker build --no-cache --label git_commit=${GIT_COMMIT} --label git_branch=${GIT_BRANCH} --ssh default -f Dockerfile -t ${DOCKER_REGISTRY}picard/prime:${IMAGE_VERSION} .

njsscan:
	docker run -it --rm --entrypoint "" -v $(shell pwd):/tmp/bdoc:ro opensecurity/njsscan:latest bash -c "echo -n 'Source files scanned with ' && njsscan -v; njsscan /tmp/bdoc"

install:
	yarn

clean:
	rm -rf ./node_modules

reinstall: clean install

dev:
	NODE_ENV=development yarn start

lint:
	yarn lint

test:
	NODE_ENV=test yarn test

snapshots:
	yarn update-snapshots

test-coverage:
	yarn coverage
