# syntax=docker/dockerfile:experimental

# Base image
FROM 017325732341.dkr.ecr.us-east-1.amazonaws.com/stigd/node:14 as production-stage

# Set the application directory
WORKDIR /usr/src/app

# Install build dependencies
RUN yum install -y openssh-clients git \
 && yum clean all

# Add GitLab to Known Hosts
RUN --mount=type=ssh mkdir -p -m 0600 ~/.ssh \
 && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

# Copy dependencies
COPY --chown=node:node package*.json yarn.lock ./

# Install production dependencies
RUN --mount=type=ssh,required=true yarn install --frozen-lockfile --prod

# Copy application context
COPY --chown=node:node . .

# Set the node user as default
USER node

# Expose port 3000
EXPOSE 3000

# Variable required to prevent 'yarn start' from exiting upon container start, stopping the container
ENV CI=true

# Start server upon container execution
CMD ["yarn", "start"]
