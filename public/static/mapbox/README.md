Mapbox GL requires a sprite sheet and font in order to work.

Typically, these are defined with `mapbox://` URL's and pulled from the internet when the application loads.

In order to support offline functioning, they've been vendored into this folder so that they can be statically served by Express.

The repository for these styles can be found at https://github.com/mapbox/mapbox-gl-styles and the license is included within this folder per requirements.
