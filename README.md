# Configuration

### Environment Configuration

> **Note**: Update `REACT_APP_PAPI_URL` to point to you PAPI instance

```bash
cp .env.example .env
```

# Deployment

## Without Docker

### Prerequisites

- Access to an instance of [PAPI](https://gitlab.com/novetta/picard/papi)
- **Environment configured as outlined in the [Environment Configuration](#environment-configuration) section above**
- [Node.js](https://nodejs.org/en/) installed
- [Yarn](https://classic.yarnpkg.com/en/docs/install) installed
- [Make for Windows](http://gnuwin32.sourceforge.net/packages/make.htm) installed, if on Windows

### Step 1: Install Dependencies

```bash
make install
```

### Step 2: Start the development server

```bash
make dev
```

## With Docker

### Prerequisites

- Access to an instance of [PAPI](https://gitlab.com/novetta/picard/papi)
- **Environment configured as outlined in the [Environment Configuration](#environment-configuration) section above**
- [Docker](https://docs.docker.com/get-docker/) >= 18.09
- [docker-compose](https://docs.docker.com/compose/install/) >= 3.7
- [`ssh` keys in GitLab](https://docs.gitlab.com/ee/ssh/)
- [`ssh-agent` with keys loaded](https://www.ssh.com/ssh/agent)
- [Enable Buildkits in Docker](https://docs.docker.com/develop/develop-images/build_enhancements/). **Already enabled when using `make` to build the Docker image**

### Prerequisites for Building Production Images

The production image uses our STIGd NodeJS image as a base. Therefore, you must be logged into the ECR registry to pull the image, requiring the following:

- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- IAM **read** permissions to the ECR registry
- AWS credentials under `~/.aws/credentials`
- Logged in to the ECR registry:

`aws ecr get-login-password --region us-gov-west-1 | docker login --username AWS --password-stdin 222262090337.dkr.ecr.us-gov-west-1.amazonaws.com`

> The previous command provides an authorization token **valid for 12 hours**. After those 12 hours, you must run the command again to regain access

### **Development** Docker Image

```bash
make dist            # build image
docker-compose up -d # create and start container and network
```

### **Production** Docker Image

```bash
make dist-prod       # build image
docker-compose up -d # create and start container and network
```

### Teardown

Stop and remove container and network

```bash
docker-compose down
```

## BDOC's CI/CD Pipeline

```mermaid
gantt
	title BDOC's CI/CD Pipeline
	dateFormat  YYYY-MM-DD.HH.mm.ss
	axisformat %M.%S min
	section Build
	install-dependencies :a1, 2020-01-01.00.00.00, 115s
	section Static-analysis
	njsscan : 2020-01-01.00.00.00, 71s
	eslint : b1, after a1, 40s
	section Test
	react-test: c1, after b1, 48s
	section Build-image
	build-and-deliver: d1, after c1, 465s
	section Scan-image
	clair-scan: e1, after d1, 97s
	section Deliver-image
	image-delivery-base: after e1, 188s
	manual-delivery-dev: after e1, 30s
```

> This pipeline imports generic jobs and templates from our [devsecops/gitlab-ci-templates](https://gitlab.com/novetta/picard/devsecops/gitlab-ci-templates) centralized repository.

### Stage 1: Build

#### `install-dependencies`

Using `yarn`, installs all the dependencies specified in the `package.json` file of the project

### Stage 2: Static Analysis

#### `eslint-test`

Static analysis with the use of `eslint` to find problems in the `JavaScript` code

#### `njsscan-sast-test`

Static application security testing (SAST) with the use of `njsscan` in order to find insecure `NodeJS` code patterns

### Stage 3: Test

#### `react-test`

Integration testing

### Stage 4: Build Image

#### `build-and-deliver`

- Logs into the Amazon ECR Registry
- Builds a `PRIME` Docker image
- Pushes the built Docker image into the Amazon ECR Registry with a `scan-<commit>` tag for scanning with `CoreOS Clair`

### Stage 5: Scan Image

#### `interpret-clair-scan-results`

Verifies the `CoreOS Clair` Scan Results using our [image-scan-verifier](https://gitlab.com/novetta/picard/devsecops/image-scan-verifier). If the `CoreOS Clair` Image Scan Results finds a non-whitelisted **HIGH** or **CRITICAL**
vulnerability, the build fails.

### Stage 6: Deliver Image

> Use environment variable `VERSION` to specify the Docker image version. Otherwise:
>
> - If it's a Git `tag`, it will use the specified tag
> - If none of the previous applies, the short commit `sha` will be used with the format `dev-<sha>`

#### `.image-delivery-template`

Template used for delivering the built image to a base according to the `AWS_ACCOUNT_ID`, `AWS_DEFAULT_REGION`, `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` variables passed in the job.

#### `image-delivery:<baseID>`

> Executed **ONLY** when Git a `tag` is created

Deliver the image to the specified base by extending `.image-delivery-template` and assigning the base's `AWS_ACCOUNT_ID`, `AWS_DEFAULT_REGION`, `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` variables.

#### `.retag-image-template`

Template used for retagging the already delivered image in `Dev` with the `scan-<commit>` tag.

> There is already a `LifecyclePolicy` in Amazon ECR that will delete these `scan-<commit>` tags on a daily basis

#### `image-delivery:dev`

> `Dev` delivery executed **ONLY** on `schedules` and Git `tags`

Extends the `.retag-image-template`, which retags the original image pushed for the scanning with the specified version.

#### `manual-image-delivery:dev`

Retag the image on `Dev` by extending `.retag-image-template`, which retags the original image pushed for the scanning with the specified version.
